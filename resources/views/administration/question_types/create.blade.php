@extends('layouts.master')
@section('title', 'Create role - Roles and permissions - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration">Administration</a></li>
        <li><a href="/administration/question_types">Question types</a></li>
        <li class="current">Create question type</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for creating question types. -->
        <h1>Create question type</h1>
        {!! Form::open(array('action' => 'QuestionTypeController@store', 'id' => 'createQuestionType')) !!}
            {!! csrf_field() !!}
            <!-- Type field. -->
            <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Name of Question Type
                    <div class="col-md-6">
                        @if ($errors->has('type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="type" value="{{ old('type') }}">
                    </div>
                </label>
            </div>
            <!-- Submit form to create question type. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Create question type
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection