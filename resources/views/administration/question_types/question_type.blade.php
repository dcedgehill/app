@extends('layouts.master')
@section('title', $questiontype->type . ' - Question types - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration">Administration</a></li>
        <li><a href="/administration/question_types">Question types</a></li>
        <li class="current">{{ $questiontype->type }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the question type. -->
        <h1>{{ $questiontype->type }}</h1>
        <!-- Display edit and delete buttons for editing and deleting the question type -->
        <p id="editBtn"><a href="/administration/question_types/{{ $questiontype->id }}/edit" class="button">Edit question type</a></p>
        {!! Form::model($questiontype, ['id' => 'deleteQuestionType', 'onsubmit' => 'return confirm_delete("question type")', 'method' => 'DELETE', 'url' => 'administration/question_types/' . $questiontype->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete question type
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection