@extends('layouts.master')
@section('title', 'Edit ' . $role->name . ' role - Roles and permissions - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li><a href="/administration/roles/{{ $role->id }}" >{{ $role->name }}</a></li>
        <li class="current">Edit {{ $role->name }} role</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for editing roles. -->
        <h1>Edit {{ $role->name }} role</h1>
        {!! Form::model($role, ['id' => 'editRole', 'method' => 'PATCH', 'url' => 'administration/roles/' . $role->id]) !!}
            {!! csrf_field() !!}
            <!-- Name field. -->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Name of Role
                    <div class="col-md-6">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </label>
            </div>
            <!-- Submit form to update role. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Edit role
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection