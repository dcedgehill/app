@extends('layouts.master')
@section('title', 'Create role - Roles and permissions - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li class="current">Create role</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for creating roles. -->
        <h1>Create role</h1>
        {!! Form::open(array('action' => 'RoleController@store', 'id' => 'createRole')) !!}
            {!! csrf_field() !!}
            <!-- Name field. -->
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Name of Role
                    <div class="col-md-6">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </label>
            </div>
            <!-- Submit form to create role. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Create role
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection