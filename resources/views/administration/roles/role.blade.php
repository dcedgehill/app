@extends('layouts.master')
@section('title', $role->name . ' - Roles and permissions - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li class="current">{{ $role->name }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the role and their assigned permissions as long as at least one permission has been assigned. -->
        <h1>{{ $role->name }}</h1>
        @if (count($role->permissions) > 0)
            <ul>
                @foreach ($role->permissions as $permission)
                    <li>{{ $permission->permission }}</li>
                @endforeach
            </ul>
        @else
            <p>No permissions have been assigned yet to this role.</p>
        @endif
        <!-- Displays the edit and delete buttons for editing and deleting the role -->
        <p id="editBtn"><a href="/administration/roles/{{ $role->id }}/edit" class="button">Edit role</a></p>
        {!! Form::model($role, ['id' => 'deleteRole', 'onsubmit' => 'return confirm_delete("role and its permissions")', 'method' => 'DELETE', 'url' => 'administration/roles/' . $role->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete role
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection