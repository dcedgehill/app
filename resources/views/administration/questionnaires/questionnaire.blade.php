@extends('layouts.master')
@section('title', $questionnaire->title . ' - Questionnaires - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/questionnaires" >Questionnaires</a></li>
        <li class="current">{{ $questionnaire->title }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the questionnaire and its relevant information. -->
        <h1>{{ $questionnaire->title }}</h1>
        <?php
        /**
         * Convert the questionnaire dates into times.
         */
        $questionnaire->start_date = strtotime($questionnaire->start_date);
        $questionnaire->end_date = strtotime($questionnaire->end_date);
        ?>
        <!-- Displays the status of the questionnaire based on the current date and time. -->
        <p>
            @if (date('Y-m-d H:i', $questionnaire->start_date) > date('Y-m-d H:i', time()))
                Not publicly available yet
            @elseif (date('Y-m-d H:i', $questionnaire->end_date) > date('Y-m-d H:i', time()))
                Publicly available
            @else
                No longer publicly available
            @endif | Starting date: {{ date('Y F jS l - H:i', $questionnaire->start_date) }} | Ending date: {{ date('Y F jS l - H:i', $questionnaire->end_date) }}
        </p>
        <!-- Questionnaire owner. -->
        <p>Created by: {{ $questionnaire->user->email }}</p>
        <!-- Questionnaire description. -->
        <p>{{ $questionnaire->description }}</p>
        <!-- Questionnaire ethical considerations. -->
        <p>{{ $questionnaire->ethical_considerations }}</p>
        <p id="questionsAnswersBtn"><a href="/administration/questionnaires/{{ $questionnaire->id }}/questions" class="button">View questions and answers</a></p>
        <!-- Display delete button for deleting the questionnaire. -->
        {!! Form::model($questionnaire, ['id' => 'deleteQuestionnaire', 'onsubmit' => 'return confirm_delete("questionnaire")', 'method' => 'DELETE', 'url' => '/administration/questionnaires/' . $questionnaire->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete questionnaire
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection