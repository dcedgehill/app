@extends('layouts.master')
@section('title', 'Questions - ' . $questionnaire->title . ' - Questionnaires - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/questionnaires" >Questionnaires</a></li>
        <li><a href="/administration/questionnaires/{{ $questionnaire->id }}" >{{ $questionnaire->title }}</a></li>
        <li class="current">Questions and answers</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the questionnaire's questions as long as at least one question has been created for it. -->
        <h1>Questions and answers - {{ $questionnaire->title }}</h1>
        @if (count($questionnaire->questions) > 0)
            @foreach ($questionnaire->questions->sortBy('position_number') as $question)
                <a id="question{{ $question->id }}" href="/administration/questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">#{{ $question->position_number }} {{ $question->question }}</h3>
                        <p>Type: {{ $question->questiontype->type }}</p>
                    </div>
                </a>
            @endforeach
        @else
            <p>No questions and answers have been created for this questionnaire.</p>
        @endif
    </section>
@endsection