@extends('layouts.master')
@section('title', $question->question . ' - ' . $questionnaire->title . ' - Questionnaires - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration">Administration</a></li>
        <li><a href="/administration/questionnaires">Questionnaires</a></li>
        <li><a href="/administration/questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li><a href="/administration/questionnaires/{{ $questionnaire->id }}/questions">Questions and answers</a></li>
        <li class="current">{{ $question->question }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the question and its type. -->
        <h1>#{{ $question->position_number }} {{ $question->question }}</h1>
        <p>Type: {{ $question->questiontype->type }}</p>
        @if ($question->questiontype->type != "Open-ended")
            <p id="answersBtn"><a href="/administration/questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers" class="button">View answers</a></p>
        @endif
    </section>
@endsection