@extends('layouts.master')
@section('title', 'Answers - ' . $question->question . ' - ' . $questionnaire->title . ' - Questionnaires - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration">Administration</a></li>
        <li><a href="/administration/questionnaires">Questionnaires</a></li>
        <li><a href="/administration/questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li><a href="/administration/questionnaires/{{ $questionnaire->id }}/questions">Questions and answers</a></li>
        <li><a href="/administration/questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">{{ $question->question }}</a></li>
        <li class="current">Answers</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the question's answers as long as at least one answer has been created for it. -->
        <h1>Answers - {{ $question->question }}</h1>
        @if (count($question->answers) > 0)
            @foreach ($question->answers->sortBy('position_number') as $answer)
                <div class="panel">
                    <h3>#{{ $answer->position_number }} {{ $answer->answer }}</h3>
                </div>
            @endforeach
        @else
            <p>No answers have been created for this question.</p>
        @endif
    </section>
@endsection