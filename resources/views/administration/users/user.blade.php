@extends('layouts.master')
@section('title', $showUser->email . ' user - Users - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/users" >Users</a></li>
        <li class="current">{{ $showUser->email }} user</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the user and their relevant information. -->
        <h1>{{ $showUser->email }} user</h1>
        <ul>
            <li>ID: {{ $showUser->id }}</li>
            <li>Role(s):
                <ul>
                    @foreach ($showUser->roles as $role)
                        <li>{{ $role->name }}</li>
                    @endforeach
                </ul>
            </li>
            <!-- Converts a timestamp into something as "2016 May 7th Saturday - 14:09:37". -->
            <li>Account creation: {{ $user->created_at->format('Y F jS l - H:i:s') }}</li>
            <li>Last logged in: {{ $user->updated_at->format('Y F jS l - H:i:s') }}</li>
        </ul>
        <!-- Displays the edit and delete buttons for editing and deleting the user -->
        <p id="editBtn"><a href="/administration/users/{{ $showUser->id }}/edit" class="button">Assign role(s) to user</a></p>
        {!! Form::model($showUser, ['id' => 'deleteUser', 'onsubmit' => 'return confirm_delete("user and its questionnaires")', 'method' => 'DELETE', 'url' => 'administration/users/' . $showUser->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete user
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection