@extends('layouts.master')
@section('title', 'Edit ' . $editUser->email . ' user - Users - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/users" >Users</a></li>
        <li><a href="/administration/users/{{ $editUser->id }}" >{{ $editUser->email }}</a></li>
        <li class="current">Assign role(s) to {{ $editUser->email }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for assigning roles to users. -->
        <h1>Assign role(s) to {{ $editUser->email }}</h1>
        {!! Form::model($editUser, ['id' => 'editUser', 'method' => 'PATCH', 'url' => 'administration/users/' . $editUser->id]) !!}
            {!! csrf_field() !!}
            <!-- Update which users to assign the role to. -->
            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Assign to which Role(s)?</label>
                @if ($errors->has('role'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                @endif
                <div class="col-md-6">
                    @foreach($roles as $role)
                        <label class="floatMultipleOptions" class="col-md-4 control-label">{{ $role->name }}
                            {{ Form::checkbox('role[]', $role->id, $editUser->roles->contains($role->id), ['id' => $role->id]) }}
                        </label>
                    @endforeach
                    <div id="clearLeft"></div>
                </div>
            </div>
            <!-- Submit form to update user. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Edit user
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection