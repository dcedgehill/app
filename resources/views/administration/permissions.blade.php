@extends('layouts.master')
@section('title', 'List of permissions - Roles and permissions - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li class="current">List of permissions</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the permissions and their assigned roles as long as at least one permission and role has been created. -->
        <h1>List of permissions</h1>
        <p><a href="/administration/permissions/create" class="button">Create permission</a>
        @if (count($permissions) > 0)
            @foreach ($permissions as $permission)
                <a id="permission{{ $permission->id }}" href="/administration/permissions/{{ $permission->id }}">
                    <div id="{{ $permission->permission }}" class="panel">
                        <h3 id="clickableLink">{{ $permission->permission }}</h3>
                        @if (count($permission->roles) > 0)
                            <ul>
                                @foreach ($permission->roles as $role)
                                    <li>{{ $role->name }}</li>
                                @endforeach
                            </ul>
                        @else
                            <p>No roles have this permission assigned to them.</p>
                        @endif
                    </div>
                </a>
            @endforeach
        @else
            <p>No permissions have been created.</p>
        @endif
    </section>
@endsection