@extends('layouts.master')
@section('title', $permission->permission . ' - List of permissions - Roles and permissions - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li><a href="/administration/permissions" >List of permissions</a></li>
        <li class="current">{{ $permission->permission }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the permission's roles as long as at least one role has been assigned to it. -->
        <h1>{{ $permission->permission }}</h1>
        @if (count($permission->roles) > 0)
            <ul>
                @foreach ($permission->roles as $role)
                    <li>{{ $role->name }}</li>
                @endforeach
            </ul>
        @else
            <p>No roles have this permission assigned to them.</p>
        @endif
        <!-- Display edit and delete buttons for editing and deleting the permission -->
        <p id="editBtn"><a href="{{ $permission->id }}/edit" class="button">Edit permission</a></p>
        {!! Form::model($permission, ['id' => 'deletePermission', 'onsubmit' => 'return confirm_delete("permission")', 'method' => 'DELETE', 'url' => 'administration/permissions/' . $permission->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete permission
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection