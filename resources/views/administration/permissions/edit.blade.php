@extends('layouts.master')
@section('title', 'Edit ' . $permission->permission . ' permission - List of permissions - Roles and permissions - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li><a href="/administration/permissions" >List of permissions</a></li>
        <li><a href="/administration/permissions/{{ $permission->id }}" >{{ $permission->permission }}</a></li>
        <li class="current">Edit {{ $permission->permission }} permission</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for editing permissions. -->
        <h1>Edit {{ $permission->permission }} permission</h1>
        {!! Form::model($permission, ['id' => 'editPermission', 'method' => 'PATCH', 'url' => 'administration/permissions/' . $permission->id]) !!}
            {!! csrf_field() !!}
            <!-- Permission field. -->
            <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Name of Permission (leave to not change the permission name)
                    <div class="col-md-6">
                        @if ($errors->has('permission'))
                            <span class="help-block">
                                <strong>{{ $errors->first('permission') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="permission" value="@if(!old('permission')){{$permission->permission }}@else{{old('permission')}}@endif">
                    </div>
                </label>
            </div>
            <!-- Update which roles to assign the permission to. -->
            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Assign to which Role(s)?</label>
                @if ($errors->has('role'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                @endif
                <div class="col-md-6">
                    @foreach($roles as $role)
                        <label class="floatMultipleOptions" class="col-md-4 control-label">{{ $role->name }}
                            {{ Form::checkbox('role[]', $role->id, $permission->roles->contains($role->id), ['id' => $role->id]) }}
                        </label>
                    @endforeach
                    <div id="clearLeft"></div>
                </div>
            </div>
            <!-- Submit form to update permission. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Edit permission
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection