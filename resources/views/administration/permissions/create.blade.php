@extends('layouts.master')
@section('title', 'Create permission - List of permissions - Roles and permissions - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li><a href="/administration/roles" >Roles and permissions</a></li>
        <li><a href="/administration/permissions" >List of permissions</a></li>
        <li class="current">Create permission</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for creating permissions. -->
        <h1>Create permission</h1>
        {!! Form::open(array('action' => 'PermissionController@store', 'id' => 'createPermission')) !!}
            {!! csrf_field() !!}
            <!-- Permission field. -->
            <div class="form-group{{ $errors->has('permission') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Name of Permission
                    <div class="col-md-6">
                        @if ($errors->has('permission'))
                            <span class="help-block">
                                <strong>{{ $errors->first('permission') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="permission" value="{{ old('permission') }}">
                    </div>
                </label>
            </div>
            <!-- Check which roles to assign the permission to. -->
            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Assign to which Role(s)?</label>
                @if ($errors->has('role'))
                    <span class="help-block">
                        <strong>{{ $errors->first('role') }}</strong>
                    </span>
                @endif
                <div class="col-md-6">
                    @foreach($roles as $role)
                        <label class="floatMultipleOptions" class="col-md-4 control-label">{{ $role->name }}
                            {{ Form::checkbox('role[]', $role->id, null, ['id' => $role->id]) }}
                        </label>
                    @endforeach
                    <div id="clearLeft"></div>
                </div>
            </div>
            <!-- Submit form to create permission. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Create permission
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection