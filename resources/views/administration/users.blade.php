@extends('layouts.master')
@section('title', 'Users - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li class="current">Users</li>
    </ul>
    @if (Session::has('statusError'))
        <p class="statusError"><strong>{{ Session::get('statusError') }}</strong></p>
    @endif
    <section class="row large-12 columns">
        <!-- Displays all of the users and their relevant information as long as at least one user exists. -->
        <h1>Users</h1>
        <p>Read, assign roles to users, and delete users.</p>
        @if (count($users) > 0)
            @foreach ($users as $per_user)
                <a id="user{{ $per_user->id }}" href="/administration/users/{{ $per_user->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">{{ $per_user->email }}</h3>
                        <ul>
                            <li>ID: {{ $per_user->id }}</li>
                            <li>Role(s):
                                <ul>
                                    @foreach ($per_user->roles as $role)
                                        <li>{{ $role->name }}</li>
                                    @endforeach
                                </ul>
                                <li>Account creation: {{ $per_user->created_at->format('Y F jS l - H:i:s') }}</li>
                                <li>Last logged in: {{ $per_user->updated_at->format('Y F jS l - H:i:s') }}</li>
                            </li>
                        </ul>
                    </div>
                </a>
            @endforeach
        @else
            <p>No users exist on this system.</p>
        @endif
    </section>
@endsection