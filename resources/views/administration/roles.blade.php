@extends('layouts.master')
@section('title', 'Roles and permissions - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li class="current">Roles and permissions</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the roles and their permissions as long as at least one role and permission has been created. -->
        <h1>Roles and permissions</h1>
        <p>Create, read, update, and delete roles, as well as for permissions belonging to existing roles.</p>
        <p><a href="/administration/roles/create" class="button">Create role</a> <a href="/administration/permissions" class="button">Access list of permissions</a>
        @if (count($roles) > 0)
            @foreach ($roles as $role)
                <a id="role{{ $role->id }}" href="/administration/roles/{{ $role->id }}">
                <div class="panel">
                    <h3 id="clickableLink">{{ $role->name }}</h3>
                    @if (count($role->permissions) > 0)
                        <ul>
                            @foreach ($role->permissions as $permission)
                                <li>{{ $permission->permission }}</li>
                            @endforeach
                        </ul>
                    @else
                        <p>No permissions have been assigned yet to this role.</p>
                    @endif
                </div>
                </a>
            @endforeach
        @else
            <p>No roles have been created.</p>
        @endif
    </section>
@endsection