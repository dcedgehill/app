@extends('layouts.master')
@section('title', 'Question types - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration">Administration</a></li>
        <li class="current">Question types</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the question types as long as at least one question type has been created for it. -->
        <h1>Question types</h1>
        <p>Create, read, update, and delete question types.</p>
        <p><a href="/administration/question_types/create" class="button">Create question type</a>
        @if (count($questiontypes) > 0)
            @foreach ($questiontypes as $questiontype)
                <a id="questionType{{ $questiontype->id }}" href="/administration/question_types/{{ $questiontype->id }}">
                    <div id="{{ $questiontype->type }}" class="panel">
                        <h3 id="clickableLink">{{ $questiontype->type }}</h3>
                    </div>
                </a>
            @endforeach
        @else
            <p>No question types have been created.</p>
        @endif
    </section>
@endsection