@extends('layouts.master')
@section('title', 'Questionnaires - Administration - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/administration" >Administration</a></li>
        <li class="current">Questionnaires</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the questionnaires and their relevant information as long as at least one questionnaire has been created. -->
        <h1>Questionnaires</h1>
        <p>Read and delete questionnaires.</p>
        @if (count($questionnaires) > 0)
            @foreach ($questionnaires->sortBy('start_date') as $questionnaire)
                <a id="questionnaire{{ $questionnaire->id }}" href="/administration/questionnaires/{{ $questionnaire->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">{{ $questionnaire->title }}</h3>
                        <?php
                        /**
                         * Convert the questionnaire dates into times.
                         */
                        $questionnaire->start_date = strtotime($questionnaire->start_date);
                        $questionnaire->end_date = strtotime($questionnaire->end_date);
                        ?>
                        <!-- Displays the status of the questionnaire based on the current date and time. -->
                        <p>
                            @if (date('Y-m-d H:i', $questionnaire->start_date) > date('Y-m-d H:i', time()))
                                Not publicly available yet
                            @elseif (date('Y-m-d H:i', $questionnaire->end_date) > date('Y-m-d H:i', time()))
                                Publicly available
                            @else
                                No longer publicly available
                            @endif | Starting date: {{ date('Y F jS l - H:i', $questionnaire->start_date) }} | Ending date: {{ date('Y F jS l - H:i', $questionnaire->end_date) }}
                        </p>
                        <!-- Questionnaire owner. -->
                        <p>Created by: {{ $questionnaire->user->email }}</p>
                        <!-- Condense the questionnaire description to a maximum of 350 characters. -->
                        <p>{{ substr($questionnaire->description, 0, 350) }}@if(strlen($questionnaire->description) > 350)...@endif</p>
                    </div>
                </a>
            @endforeach
        @else
            <p>No questionnaires have been created.</p>
        @endif
    </section>
@endsection