@extends('layouts.master')
@section('title', 'Administration - Questionnaires 4 Us')
@section('content')
    <section class="row large-12 columns">
        <!-- Displays all of the available administration pages that administrator users have access to. -->
        <h1>Administration</h1>
        <a id="questionTypes" href="/administration/question_types">
            <div class="panel">
                <h3 id="clickableLink">Question types</h3>
                <p>Create, read, update, and delete question types.</p>
            </div>
        </a>
        <a id="questionnaires" href="/administration/questionnaires">
            <div class="panel">
                <h3 id="clickableLink">Questionnaires</h3>
                <p>Read and delete questionnaires.</p>
            </div>
        </a>
        <a id="rolesAndPermissions" href="/administration/roles">
            <div class="panel">
                <h3 id="clickableLink">Roles and permissions</h3>
                <p>Create, read, update, and delete roles, as well as for permissions belonging to existing roles.</p>
            </div>
        </a>
        <a id="users" href="/administration/users">
            <div class="panel">
                <h3 id="clickableLink">Users</h3>
                <p>Read, assign roles to users, and delete users.</p>
            </div>
        </a>
    </section>
@endsection