@extends('layouts.master')
@section('title', 'My questionnaires - Questionnaires 4 Us')
@section('content')
    <section class="row large-12 columns">
        <!-- Displays all of the user's questionnaires and their relevant information as long as the user has created at least one questionnaire. -->
        <h1>My questionnaires</h1>
        <p><a href="/my_questionnaires/create" class="button">Create questionnaire</a>
        @if (count($user->questionnaires) > 0)
            @foreach ($user->questionnaires->sortBy('start_date') as $questionnaire)
                <a id="questionnaire{{ $questionnaire->id }}" href="/my_questionnaires/{{ $questionnaire->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">{{ $questionnaire->title }}</h3>
                        <?php
                        /**
                         * Convert the questionnaire dates into times.
                         */
                        $questionnaire->start_date = strtotime($questionnaire->start_date);
                        $questionnaire->end_date = strtotime($questionnaire->end_date);
                        ?>
                        <!-- Displays the status of the questionnaire based on the current date and time. -->
                        <p>@if (date('Y-m-d H:i', $questionnaire->start_date) > date('Y-m-d H:i', time()))
                                Not publicly available yet
                            @elseif (date('Y-m-d H:i', $questionnaire->end_date) > date('Y-m-d H:i', time()))
                                Publicly available
                            @else
                                No longer publicly available
                            @endif | Starting date: {{ date('Y F jS l - H:i', $questionnaire->start_date) }} | Ending date: {{ date('Y F jS l - H:i', $questionnaire->end_date) }}</p>
                        <!-- Condense the questionnaire description to a maximum of 350 characters. -->
                        <p>{{ substr($questionnaire->description, 0, 350) }}@if(strlen($questionnaire->description) > 350)...@endif</p>
                    </div>
                </a>
            @endforeach
        @else
            <p>You have not created any questionnaires.</p>
        @endif
    </section>
@endsection