@extends('layouts.master')
@section('title', 'Results - ' .$questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}" >{{ $questionnaire->title }}</a></li>
        <li class="current">Results</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the results that the questionnaire has received as long as at it has received at least one response. -->
        <h1>Results - {{ $questionnaire->title }}</h1>
        @if (count($questionnaire->questions) > 0)
            @foreach ($questionnaire->questions->sortBy('position_number') as $question)
                <h3>#{{ $question->position_number }} {{ $question->question }}</h3>
                @if (count($question->answers) > 0)
                    <table>
                        <thead>
                            <tr>
                            @foreach ($question->answers->sortBy('position_number') as $answer)
                                <th>{{ $answer->answer }}</th>
                            @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Show the actual responses for those that are an open-ended answer. -->
                            <!-- Show number of responses for those that are not an open-ended answer. -->
                            @if ($question->questiontype->type != "Open-ended")
                                <tr>
                                    @foreach ($question->answers->sortBy('position_number') as $answer)
                                        <td>{{ count($answer->responses) }}</td>
                                    @endforeach
                                </tr>
                            @else
                                @foreach ($question->answers->sortBy('position_number') as $answer)
                                    @if (count($answer->responses) > 0)
                                        @foreach ($answer->responses as $response)
                                            <tr>
                                                <td>{{ $response->response }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td>{{ count($answer->responses) }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                @else
                    <p>You have not created any answers for this question.</p>
                @endif
            @endforeach
        @else
            <p>You have not created any questions and answers for this questionnaire.</p>
        @endif
    </section>
@endsection