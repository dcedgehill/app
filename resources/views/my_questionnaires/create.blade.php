@extends('layouts.master')
@section('title', 'Create questionnaire - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li class="current">Create questionnaire</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for creating questionnaires. -->
        <h1>Create questionnaire</h1>
        {!! Form::open(array('action' => 'MyQuestionnaireController@store', 'id' => 'createQuestionnaire')) !!}
            {!! csrf_field() !!}
            <!-- Title field. -->
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Questionnaire Title
                    <div class="col-md-6">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                    </div>
                </label>
            </div>
            <!-- Description field. -->
            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Description
                    <div class="col-md-6">
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                        <textarea class="form-control" name="description" rows="10">{{ old('description') }}</textarea>
                    </div>
                </label>
            </div>
            <!-- Ethical considerations field. -->
            <div class="form-group{{ $errors->has('ethical_considerations') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Ethical Considerations
                    <div class="col-md-6">
                        @if ($errors->has('ethical_considerations'))
                            <span class="help-block">
                                <strong>{{ $errors->first('ethical_considerations') }}</strong>
                            </span>
                        @endif
                        <textarea class="form-control" name="ethical_considerations" rows="10">{{ old('ethical_considerations') }}</textarea>
                    </div>
                </label>
            </div>
            <!-- Select a starting date for the questionnaire. -->
            <div class="form-group{{ $errors->has('date_start') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Starting Date of Questionnaire ("2016-04-10", "2016-04-10 18:00")
                    <div class="col-md-6">
                        @if ($errors->has('start_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                        @endif
                        <input id="date_start" type="text" class="form-control" name="start_date" value="{{ old('start_date') }}">
                    </div>
                </label>
            </div>
            <!-- Select an ending date for the questionnaire -->
            <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Ending Date of Questionnaire ("2016-05-10", "2016-05-10 18:00")
                    <div class="col-md-6">
                        @if ($errors->has('end_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('end_date') }}</strong>
                            </span>
                        @endif
                        <input id="date_end" type="text" class="form-control" name="end_date" value="{{ old('end_date') }}">
                    </div>
                </label>
            </div>
            <!-- Submit form to create questionnaire. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Create questionnaire
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection