@extends('layouts.master')
@section('title', 'Questions - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}" >{{ $questionnaire->title }}</a></li>
        <li class="current">Questions and answers</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the questionnaire's questions and their relevant information as long as at least one question exists for it. -->
        <h1>Questions and answers - {{ $questionnaire->title }}</h1>
        <p><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/create" class="button">Create question</a>
        @if (count($questionnaire->questions) > 0)
            @foreach ($questionnaire->questions->sortBy('position_number') as $question)
                <a id="question{{ $question->id }}" href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">#{{ $question->position_number }} {{ $question->question }}</h3>
                        <p>Type: {{ $question->questiontype->type }}</p>
                        @if ($question->position_number != 1)
                            <a href="/my_questionnaires/{{ $questionnaire->id }}/questions/swap_position/{{ $question->position_number  }}/with/{{ $question->position_number - 1 }}" class="button small">Move question up</a>
                        @endif
                        @if ($question->position_number < count($questionnaire->questions))
                            <a href="/my_questionnaires/{{ $questionnaire->id }}/questions/swap_position/{{ $question->position_number  }}/with/{{ $question->position_number + 1 }}" class="button small">Move question down</a>
                        @endif
                        <div id="clearLeft"></div>
                        {!! Form::close() !!}
                    </div>
                </a>
            @endforeach
        @else
            <p>You have not created any questions and answers for this questionnaire.</p>
        @endif
    </section>
@endsection