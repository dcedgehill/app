@extends('layouts.master')
@section('title', 'Edit ' . $question->question . ' question - ' . $question->question . ' - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions" >Questions and answers</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">{{ $question->question }}</a></li>
        <li class="current">Edit {{ $question->question }} question</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for updating questions. -->
        <h1>Edit {{ $question->question }} question</h1>
        {!! Form::model($question, ['id' => 'editQuestion', 'method' => 'PATCH', 'url' => 'my_questionnaires/' . $questionnaire->id . '/questions/' . $question->id]) !!}
            {!! csrf_field() !!}
            <!-- Question field. -->
            <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Question
                    <div class="col-md-6">
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                        <input list="questions" class="form-control" name="question" autocomplete="off" value="@if(!old('question')){{$question->question}}@else{{old('question')}}@endif">
                        <datalist id="questions">
                            @foreach ($questions->sortBy('question') as $questionOption)
                                @if ($user->questionnaires != $questionnaire)
                                    <option value="{{ $questionOption->question }}">
                                @endif
                            @endforeach
                        </datalist>
                    </div>
                </label>
            </div>
            <!-- Select a question type. -->
            <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Type
                    <div class="col-md-6">
                        @if ($errors->has('type_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type_id') }}</strong>
                            </span>
                        @endif
                        <input type="text" disabled class="form-control" value="{{ $question->questiontype->type }}">
                    </div>
                </label>
            </div>
            <!-- Submit form to update question. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Edit question
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection