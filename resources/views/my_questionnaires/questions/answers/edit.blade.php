@extends('layouts.master')
@section('title', 'Edit ' . $answer->answer . ' answer - ' . $question->question . ' - Questions - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires">My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions" >Questions and answers</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">{{ $question->question }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers" >Answers</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers/{{ $answer->id }}">{{ $answer->answer }}</a></li>
        <li class="current">Edit {{ $answer->answer }} answer</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for updating answers. -->
        <h1>Edit {{ $answer->answer }} answer</h1>
        {!! Form::model($answer, ['id' => 'editAnswer', 'method' => 'PATCH', 'url' => 'my_questionnaires/' . $questionnaire->id . '/questions/' . $question->id . '/answers/' . $answer->id]) !!}
            {!! csrf_field() !!}
            <!-- Answer field -->
            <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Answer
                    <div class="col-md-6">
                        @if ($errors->has('answer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('answer') }}</strong>
                            </span>
                        @endif
                        <input list="answers" class="form-control" name="answer" value="@if(!old('answer')){{$answer->answer}}@else{{old('answer')}}@endif">
                        <datalist id="answers">
                            @foreach ($answers->sortBy('answer') as $answer)
                                @if ($user->questionnaires != $questionnaire)
                                    <option value="{{ $answer->answer }}">
                                @endif
                            @endforeach
                        </datalist>
                    </div>
                </label>
            </div>
            <!-- Submit form to update answer. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Edit answer
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection