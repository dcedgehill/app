@extends('layouts.master')
@section('title', 'Create answer - Answers - ' . $question->question . ' - Questions - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires">My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions">Questions</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">{{ $question->question }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers" >Answers</a></li>
        <li class="current">Create question</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for creating answers. -->
        <h1>Create answer</h1>
        {!! Form::open(array('action' => 'AnswerController@store', 'url' => 'my_questionnaires/' . $questionnaire->id . '/questions/' . $question->id . '/answers', 'id' => 'createAnswer')) !!}
            {!! csrf_field() !!}
            <!-- Answer field. -->
            <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Answer
                    <div class="col-md-6">
                        @if ($errors->has('answer'))
                            <span class="help-block">
                                <strong>{{ $errors->first('answer') }}</strong>
                            </span>
                        @endif
                        <input list="answers" class="form-control" name="answer" autocomplete="off" value="{{ old('answer') }}">
                        <datalist id="answers">
                            @foreach ($answers->sortBy('answer') as $answer)
                                @if ($user->questionnaires != $questionnaire)
                                    <option value="{{ $answer->answer }}">
                                @endif
                            @endforeach
                        </datalist>
                    </div>
                </label>
            </div>
            <!-- Submit form to create answer. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Create answer
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection