@extends('layouts.master')
@section('title', '' . $answer->answer . ' - ' . $question->question . ' - Questions - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires">My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions" >Questions and answers</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">{{ $question->question }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers" >Answers</a></li>
        <li class="current">{{ $answer->answer }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the answer. -->
        <h1>#{{ $answer->position_number }} {{ $answer->answer }}</h1>
        <!-- Displays the edit and delete buttons for editing and deleting the answer -->
        <p id="editBtn"><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers/{{ $answer->id }}/edit" class="button">Edit answer</a></p>
        {!! Form::model($answer, ['id' => 'deleteAnswer', 'onsubmit' => 'return confirm_delete("answer")', 'method' => 'DELETE', 'url' => '/my_questionnaires/' . $questionnaire->id . '/questions/' . $question->id . '/answers/' . $answer->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete answer
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection