@extends('layouts.master')
@section('title', '' . $question->question . ' - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}" >{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions" >Questions and answers</a></li>
        <li class="current">{{ $question->question }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays the question and its type. -->
        <h1>#{{ $question->position_number }} {{ $question->question }}</h1>
        <p>Type: {{ $question->questiontype->type }}</p>
        @if ($question->questiontype->type != "Open-ended")
            <p id="answersBtn"><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers" class="button">View answers</a></p>
        @endif
        <!-- Displays the edit and delete buttons for editing and deleting the question -->
        <p id="editBtn"><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/edit" class="button">Edit question</a></p>
        {!! Form::model($question, ['id' => 'deleteQuestion', 'onsubmit' => 'return confirm_delete("question")', 'method' => 'DELETE', 'url' => '/my_questionnaires/' . $questionnaire->id . '/questions/' . $question->id]) !!}
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary alert">
                        <i class="fa fa-btn"></i>Delete question
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection