@extends('layouts.master')
@section('title', 'Answers - ' . $question->question . ' - Questions - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}" >{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions">Questions</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}">{{ $question->question }}</a></li>
        <li class="current">Answers</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays all of the question's answer and their relevant information as long as at least one answer exists for it. -->
        <h1>Answers - {{ $question->question }}</h1>
        <p><a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers/create" class="button">Create answer</a>
        @if (count($question->answers) > 0)
            @foreach ($question->answers->sortBy('position_number') as $answer)
                <a id="answer{{ $answer->id }}" href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers/{{ $answer->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">#{{ $answer->position_number }} {{ $answer->answer }}</h3>
                        @if ($answer->position_number != 1)
                            <a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers/swap_position/{{ $answer->position_number }}/with/{{ $answer->position_number - 1 }}" class="button small">Move answer up</a>
                        @endif
                        @if ($answer->position_number < count($question->answers))
                            <a href="/my_questionnaires/{{ $questionnaire->id }}/questions/{{ $question->id }}/answers/swap_position/{{ $answer->position_number }}/with/{{ $answer->position_number + 1 }}" class="button small">Move answer down</a>
                        @endif
                        <div id="clearLeft"></div>
                    </div>
                </a>
            @endforeach
        @else
            <p>You have not created any answers for this question yet.</p>
        @endif
    </section>
@endsection