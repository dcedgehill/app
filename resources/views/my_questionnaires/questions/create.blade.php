@extends('layouts.master')
@section('title', 'Create question - ' . $questionnaire->title . ' - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}" >{{ $questionnaire->title }}</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}/questions">Questions and answers</a></li>
        <li class="current">Create question</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for creating questions. -->
        <h1>Create question</h1>
        {!! Form::open(array('action' => 'QuestionController@store', 'url' => 'my_questionnaires/' . $questionnaire->id . '/questions', 'id' => 'createQuestion')) !!}
            {!! csrf_field() !!}
            <!-- Question field. -->
            <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Question
                    <div class="col-md-6">
                        @if ($errors->has('question'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question') }}</strong>
                            </span>
                        @endif
                        <input list="questions" class="form-control" name="question" autocomplete="off" value="{{ old('question') }}">
                        <datalist id="questions">
                            @foreach ($questions->sortBy('question') as $question)
                                @if ($user->questionnaires != $questionnaire)
                                    <option value="{{ $question->question }}">
                                @endif
                            @endforeach
                        </datalist>
                    </div>
                </label>
            </div>
            <!-- Select a question type. -->
            <div class="form-group{{ $errors->has('type_id') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Type
                    <div class="col-md-6">
                        @if ($errors->has('type_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('type_id') }}</strong>
                            </span>
                        @endif
                        <select class="form-control" name="type_id">
                            <option value="{{ old('type_id') }}">Select a type for this question</option>
                            @foreach($questiontypes as $questiontype)
                                @if(old('type_id') == $questiontype->id)<option value="{{ $questiontype->id }}" selected>{{ $questiontype->type }}</option>@else<option value="{{ $questiontype->id }}">{{ $questiontype->type }}</option>@endif
                            @endforeach
                        </select>
                    </div>
                </label>
            </div>
            <!-- Submit form to create question. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Create question
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection