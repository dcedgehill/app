@extends('layouts.master')
@section('title', 'Edit ' . $questionnaire->title . ' questionnaire - My questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/my_questionnaires" >My questionnaires</a></li>
        <li><a href="/my_questionnaires/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li class="current">Edit {{ $questionnaire->title }} questionnaire</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for updating questionnaires. -->
        <h1>Edit {{ $questionnaire->title }} questionnaire</h1>
        {!! Form::model($questionnaire, ['id' => 'editQuestionnaire', 'method' => 'PATCH', 'url' => 'my_questionnaires/' . $questionnaire->id]) !!}
            {!! csrf_field() !!}
            <!-- Title field. -->
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Questionnaire Title
                    <div class="col-md-6">
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="title" value="@if(!old('title')){{$questionnaire->title }}@else{{old('title')}}@endif">
                    </div>
                </label>
            </div>
            <!-- Description field. -->
            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Description
                    <div class="col-md-6">
                        @if ($errors->has('description'))
                            <span class="help-block">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                        <textarea class="form-control" name="description" rows="10">@if(!old('description')){{$questionnaire->description }}@else{{old('description')}}@endif</textarea>
                    </div>
                </label>
            </div>
            <!-- Ethical considerations field. -->
            <div class="form-group{{ $errors->has('ethical_considerations') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Ethical Considerations
                    <div class="col-md-6">
                        @if ($errors->has('ethical_considerations'))
                            <span class="help-block">
                                <strong>{{ $errors->first('ethical_considerations') }}</strong>
                            </span>
                        @endif
                        <textarea class="form-control" name="ethical_considerations" rows="10">@if(!old('ethical_considerations')){{$questionnaire->ethical_considerations }}@else{{old('ethical_considerations')}}@endif</textarea>
                    </div>
                </label>
            </div>
            <!-- Select a starting date for the questionnaire. -->
            <div class="form-group{{ $errors->has('date_start') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Starting Date of Questionnaire ("2016-04-10", "2016-04-10 18:00")
                    <div class="col-md-6">
                        @if ($errors->has('start_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('start_date') }}</strong>
                            </span>
                        @endif
                        <input id="date_start" type="text" class="form-control" name="start_date" value="@if(!old('start_date')){{$questionnaire->start_date }}@else{{old('start_date')}}@endif">
                    </div>
                </label>
            </div>
            <!-- Select an ending date for the questionnaire -->
            <div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Ending Date of Questionnaire ("2016-05-10", "2016-05-10 18:00")
                    <div class="col-md-6">
                        @if ($errors->has('end_date'))
                            <span class="help-block">
                                <strong>{{ $errors->first('end_date') }}</strong>
                            </span>
                        @endif
                        <input id="date_end" type="text" class="form-control" name="end_date" value="@if(!old('end_date')){{$questionnaire->end_date }}@else{{old('end_date')}}@endif">
                    </div>
                </label>
            </div>
            <!-- Submit form to update questionnaire. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Edit questionnaire
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection