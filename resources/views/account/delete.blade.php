@extends('layouts.master')
@section('title', 'Delete my account - My account - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/account/{{ $user->id }}" >My account</a></li>
        <li class="current">Delete my account</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for users to delete their accounts. -->
        <h1>Delete my account</h1>
        {!! Form::model($user, ['id' => 'deleteMyAccount', 'onsubmit' => 'return confirm_delete("account and its questionnaires")', 'method' => 'DELETE', 'url' => 'account/' . $user->id]) !!}
            {!! csrf_field() !!}
            <!-- Password field. -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Password
                    <div class="col-md-6">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input type="password" class="form-control" name="password">
                    </div>
                </label>
            </div>
            <!-- Confirm above password -->
            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Confirm Password
                    <div class="col-md-6">
                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                </label>
            </div>
            <!-- Submit form for account deletion. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Delete my account
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection