@extends('layouts.master')
@section('title', 'Update my account - My account - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/account/{{ $user->id }}" >My account</a></li>
        <li class="current">Update my account</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Form for users to update their accounts. -->
        <h1>Update my account</h1>
        {!! Form::model($user, ['id' => 'updateMyAccount', 'method' => 'PATCH', 'url' => 'account/' . $user->id]) !!}
            {!! csrf_field() !!}
            <!-- Email field. -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">New E-Mail Address (leave blank to not change your current e-mail address)
                    <div class="col-md-6">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </label>
            </div>
            <!-- Password field. -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">New Password (leave blank to not change your current password)
                    <div class="col-md-6">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input type="password" class="form-control" name="password">
                    </div>
                </label>
            </div>
            <!-- Confirm above password -->
            <div class="form-group{{ $errors->has('account_password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Confirm updated account details with your current account password
                    <div class="col-md-6">
                        @if ($errors->has('account_password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('account_password') }}</strong>
                            </span>
                        @endif
                        <input type="password" class="form-control" name="account_password">
                    </div>
                </label>
            </div>
            <!-- Submit form to update account. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Update my account
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </section>
@endsection