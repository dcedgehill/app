@extends('layouts.master')
@section('title', 'Browse questionnaires - Questionnaires 4 Us')
@section('content')
    @if ($search_results != "")
        <!-- Navigation breadcrumbs if the user is searching for specific questionnaires. -->
        <ul class="breadcrumbs">
            <li><a href="/browse">Browse questionnaires</a></li>
            <li class="current">{{ $_GET['search_query'] }}</li>
        </ul>
    @endif
    <section class="row large-12 columns">
        <!-- Displays all available or specific questionnaires. -->
        <h1>Browse questionnaires</h1>
        <!-- Search for specific questionnaires. -->
        {!! Form::open(array('action' => 'HomeController@search_browse_index', 'method' => 'GET', 'id' => 'searchQuestionnaire')) !!}
            <div class="form-group{{ $errors->has('search_query') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Search Query
                    <div class="col-md-6">
                        @if ($errors->has('search_query'))
                            <span class="help-block">
                                <strong>{{ $errors->first('search_query') }}</strong>
                            </span>
                        @endif
                        <input type="text" class="form-control" name="search_query" value="{{ old('search_query') }}">
                    </div>
                </label>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button id="searchActualBtn" type="submit" class="btn btn-primary">
                        <i class="fa fa-btn"></i>Search
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
        <!-- Search results would appear here if available. -->
        @if ($search_results != "")
            <h3>Search "<strong>{{ $_GET['search_query'] }}</strong>" | Returned <strong>({{ count($search_results) }}) search @if (count($search_results) > 1 || count($search_results) < 1) results @else result @endif</strong></h3>
            @foreach ($search_results->sortBy('start_date') as $search_result)
                <?php
                /**
                 * Convert the questionnaire dates into times.
                 */
                $search_result->start_date = strtotime($search_result->start_date);
                $search_result->end_date = strtotime($search_result->end_date);
                ?>
                <a id="questionnaire{{ $search_result->id }}" href="/questionnaire/{{ $search_result->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">{{ $search_result->title }}</h3>
                        <!-- Displays the status of the questionnaire based on the current date and time. -->
                        <p>@if (date('Y-m-d H:i', $search_result->start_date) > date('Y-m-d H:i', time()))
                                Not available yet
                            @elseif (date('Y-m-d H:i', $search_result->end_date) > date('Y-m-d H:i', time()))
                                Available
                            @else
                                No longer available
                            @endif | Starting date: {{ date('Y F jS l - H:i', $search_result->start_date) }} | Ending date: {{ date('Y F jS l - H:i', $search_result->end_date) }}</p>
                        <!-- Condense the questionnaire description to a maximum of 350 characters. -->
                        <p>{{ substr($search_result->description, 0, 350) }}@if(strlen($search_result->description) > 350)...@endif</p>
                    </div>
                </a>
            @endforeach
        @endif
        <h3>Recently available</h3>
            <p>@if (count($questionnaires) > 0)
            @foreach ($questionnaires->sortBy('start_date') as $questionnaire)
                <?php
                /**
                 * Convert the questionnaire dates into times.
                 */
                $questionnaire->start_date = strtotime($questionnaire->start_date);
                $questionnaire->end_date = strtotime($questionnaire->end_date);
                ?>
                <a id="questionnaire{{ $questionnaire->id }}" href="/questionnaire/{{ $questionnaire->id }}">
                    <div class="panel">
                        <h3 id="clickableLink">{{ $questionnaire->title }}</h3>
                        <!-- Displays the status of the questionnaire based on the current date and time. -->
                        <p>Starting date: {{ date('Y F jS l - H:i', $questionnaire->start_date) }} | Ending date: {{ date('Y F jS l - H:i', $questionnaire->end_date) }}</p>
                        <!-- Condense the questionnaire description to a maximum of 350 characters. -->
                        <p>{{ substr($questionnaire->description, 0, 350) }}@if(strlen($questionnaire->description) > 350)...@endif</p>
                    </div>
                </a>
            @endforeach
        @else
            <p>There are no questionnaires available at this time.</p>
        @endif
    </section>
@endsection