<?php
// Begin the process of minifying the HTML output.
ob_start('compress_page');
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>
        <!-- Import styling and scripts into the document template for styling and functionality for the website. -->
        <link rel="stylesheet" href="/css/app.css" />
        <script src="/js/app.js"></script>
        <script src="/js/confirm.js"></script>
    </head>
    <body>
        <!-- Header. -->
        @include('includes.header')
        <!-- Body content. -->
        @yield('content')
        <!-- Footer -->
        <footer>
            @include('includes.footer')
        </footer>
    </body>
</html>
<!-- Loads the functionality once the whole page that the user is on had loaded. -->
<script>
    $(document).foundation();
</script>
<?php
// End the process of minifying the HTML output.
ob_end_flush();

/**
 * Ensures no comments and large whitespaces are in the browser's view source window.
 */
function compress_page($buffer)
{
    $search = array(
            "/ +/" => " ",
            "/<!--\{(.*?)\}-->|<!--(.*?)-->|[\t\r\n]|<!--|-->|\/\/ <!--|\/\/ -->|<!\[CDATA\[|\/\/ \]\]>|\]\]>|\/\/\]\]>|\/\/<!\[CDATA\[/" => ""
    );
    $buffer = preg_replace(array_keys($search), array_values($search), $buffer);
    return $buffer;
}
?>