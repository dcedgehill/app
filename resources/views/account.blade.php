@extends('layouts.master')
@section('title', 'My account - Questionnaires 4 Us')
@section('content')
    @if (Session::has('statusError'))
        <p class="statusError"><strong>{{ Session::get('statusError') }}</strong></p>
    @endif
    <section class="row large-12 columns">
        <!-- Display the user account details -->
        <h1>My account</h1>
        <p>E-Mail address: {{ $user->email }}</p>
        <span>Account type(s):</span>
        <ul>
            @foreach ($user->roles as $role)
                <li>{{ $role->name }}</li>
            @endforeach
        </ul>
        <!-- Display edit and delete buttons for editing and deleting the user account details. -->
        <p>
            <a href="/account/{{ $user->id }}/edit" class="button">Update my account</a>
            <a href="/account/{{ $user->id }}/delete" class="button alert">Delete my account</a>
        </p>
    </section>
@endsection