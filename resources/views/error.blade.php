@extends('layouts.master')
@section('title', 'Questionnaires 4 Us')
@section('content')
    <section class="row large-12 columns">
        <!-- Displays a file not found error when the user has encountered a request that the system does not like.-->
        <h1>File not found</h1>
        <p>Sorry, but the system could not access the file you were looking for. This might be due to an incorrect file
            path or the file no longer existing. To avoid encountering this error, please use the links at the top of this page to
            navigate across the system.</p>
    </section>
@endsection