@extends('layouts.master')
@section('title', 'Questionnaires 4 Us')
@section('content')
    <section class="row large-12 columns">
        <!-- Home/Landing/Index page. -->
        <h1>Welcome to Questionnaires 4 Us!</h1>
        <h3>Where questionnaires are simple</h3>
        <p>Here at Questionnaires 4 Us, you have the options of either creating or partaking in a questionnaire.
            To begin creating a questionnaire, simply <a href="/register">sign up</a> or <a href="/login">sign in</a>
            on this website if you have not already done so. To partake in one, click <a href="/browse">browse</a> to
            start finding questionnaires to partake in. No sign up or sign in is required to partake in one on this
            website.</p>
    </section>
@endsection