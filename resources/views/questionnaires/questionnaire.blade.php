@extends('layouts.master')
@section('title', $questionnaire->title . ' - Browse questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/browse">Browse questionnaires</a></li>
        <li class="current">{{ $questionnaire->title }}</li>
    </ul>
    @if (Session::has('statusSuccess'))
        <p class="statusSuccess"><strong>{{ Session::get('statusSuccess') }}</strong></p>
    @endif
    <section class="row large-12 columns">
        <!-- Displays the questionnaire and its relevant information. -->
        <h1>{{ $questionnaire->title }}</h1>
        <?php
        /**
         * Convert the questionnaire dates into times.
         */
        $questionnaire->start_date = strtotime($questionnaire->start_date);
        $questionnaire->end_date = strtotime($questionnaire->end_date);
        ?>
        <!-- Displays the status of the questionnaire based on the current date and time. -->
        <p>@if (date('Y-m-d H:i', $questionnaire->start_date) > date('Y-m-d H:i', time()))
                Not available yet
            @elseif (date('Y-m-d H:i', $questionnaire->end_date) > date('Y-m-d H:i', time()))
                Available
            @else
                No longer available
            @endif | Starting date: {{ date('Y F jS l - H:i', $questionnaire->start_date) }} | Ending date: {{ date('Y F jS l - H:i', $questionnaire->end_date) }}</p>
        <!-- Questionnaire description. -->
        <p>{{ $questionnaire->description }}</p>
        <!-- Questionnaire ethical considerations. -->
        <p>{{ $questionnaire->ethical_considerations }}</p>
        <!-- Displays the partake in questionnaire button if the questionnaire is available. -->
        @if (date('Y-m-d H:i', $questionnaire->start_date) > date('Y-m-d H:i', time()))
            Sorry, but this questionnaire will not be available to partake in until {{ date('Y F jS l - H:i', $questionnaire->start_date) }}.
        @elseif (date('Y-m-d H:i', $questionnaire->end_date) > date('Y-m-d H:i', time()))
            @if (count($questionnaire->questions) > 0)
                @foreach ($questionnaire->questions->sortBy('position_number') as $question)
                    @if (count($question->answers) > 0)
                        <a href="/questionnaire/{{ $questionnaire->id }}/step/1" class="button">Partake in questionnaire</a>
                    @else
                        Sorry, but there are no answers created for this questionnaire yet.
                    @endif
                    @break
                @endforeach
            @else
                Sorry, but there are no questions created for this questionnaire yet.
            @endif
        @else
            Sorry, but this questionnaire is no longer available to partake in.
        @endif
        @for ($idx = 1; $idx <= count($questionnaire->questions); $idx++)
            {{ Session::forget('answer' . $idx) }}
        @endfor
    </section>
@endsection