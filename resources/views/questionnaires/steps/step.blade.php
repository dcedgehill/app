@extends('layouts.master')
@section('title', $question->question . ' - ' . $questionnaire->title . ' - Browse questionnaires - Questionnaires 4 Us')
@section('content')
    <!-- Navigation breadcrumbs. -->
    <ul class="breadcrumbs">
        <li><a href="/browse">Browse questionnaires</a></li>
        <li><a href="/questionnaire/{{ $questionnaire->id }}">{{ $questionnaire->title }}</a></li>
        <li class="current">{{ $question->question }}</li>
    </ul>
    <section class="row large-12 columns">
        <!-- Displays what question the participant is currently on. -->
        <h1>#{{ $question->position_number }} {{ $question->question }}</h1>
        {!! Form::open(array('action' => 'QuestionnaireController@post_step', 'url' => 'questionnaire/' . $questionnaire->id . '/step/' . $question->position_number . '/next', 'id' => 'submitResponse')) !!}
            {!! csrf_field() !!}
            <!-- Response field. -->
            <div class="form-group{{ $errors->has('response') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Type: {{ $question->questiontype->type }}
                    <div class="col-md-6">
                        @if ($errors->has('response'))
                            <span class="help-block">
                                <strong>{{ $errors->first('response') }}</strong>
                            </span>
                        @endif
                        <div class="col-md-6">
                            @if ($question->questiontype->type == "Closed-ended likert scale")
                                <!-- Closed-ended likert scale question type. -->
                                @foreach($question->answers->sortBy('position_number') as $answer)
                                    @if (Session::get('answer' . $question->position_number) == $answer->id)
                                        <label class="col-md-4 control-label floatMultipleOptions"><div class="positionAnswer">{{ $answer->answer }}</div>
                                            <div class="positionAnswer">{{ Form::radio('response', $answer->id, $answer->id, ['id' => $answer->id]) }}</div>
                                        </label>
                                    @else
                                        <label class="col-md-4 control-label floatMultipleOptions"><div class="positionAnswer">{{ $answer->answer }}</div>
                                            <div class="positionAnswer">{{ Form::radio('response', $answer->id, null, ['id' => $answer->id]) }}</div>
                                        </label>
                                    @endif
                                @endforeach
                                <div id="clearLeft"></div>
                            @elseif ($question->questiontype->type == "Closed-ended net promoter score")
                                <!-- Closed-ended net promoter score question type. -->
                                @foreach($question->answers->sortBy('position_number') as $answer)
                                    @if (Session::get('answer' . $question->position_number) == $answer->id)
                                        <label class="col-md-4 control-label floatMultipleOptions"><div class="positionAnswer">{{ $answer->answer }}</div>
                                            <div class="positionAnswer">{{ Form::radio('response', $answer->id, $answer->id, ['id' => $answer->id]) }}</div>
                                        </label>
                                    @else
                                        <label class="col-md-4 control-label floatMultipleOptions"><div class="positionAnswer">{{ $answer->answer }}</div>
                                            <div class="positionAnswer">{{ Form::radio('response', $answer->id, null, ['id' => $answer->id]) }}</div>
                                        </label>
                                    @endif
                                @endforeach
                                <div id="clearLeft"></div>
                            @elseif ($question->questiontype->type == "Closed-ended multi-select")
                                <!-- Closed-ended multi-select question type. -->
                                <?php $indexOfAnswerArray = 0; ?>
                                @foreach($question->answers->sortBy('position_number') as $answer)
                                    @if (Session::get('answer' . $question->position_number)[$indexOfAnswerArray] == $answer->id)
                                        <label class="col-md-4 control-label floatMultipleOptions"><div class="positionAnswer">{{ $answer->answer }}</div>
                                            <div class="positionAnswer">{{ Form::checkbox('response[]', $answer->id, $answer->id, ['id' => $answer->id]) }}</div>
                                        </label>
                                        <?php $indexOfAnswerArray += 1; ?>
                                    @else
                                        <label class="col-md-4 control-label floatMultipleOptions"><div class="positionAnswer">{{ $answer->answer }}</div>
                                            <div class="positionAnswer">{{ Form::checkbox('response[]', $answer->id, null, ['id' => $answer->id]) }}</div>
                                        </label>
                                    @endif
                                @endforeach
                                <div id="clearLeft"></div>
                            @elseif ($question->questiontype->type == "Open-ended")
                                <!-- Open-ended question type. -->
                                @foreach($question->answers->sortBy('position_number') as $answer)
                                    @if (Session::get('answer' . $question->position_number) == $answer->id)
                                        <textarea id="{{ $answer->id }}" class="form-control" name="response" rows="4">{{ Session::get('open-ended' . $question->position_number) }}</textarea>
                                    @else
                                        <textarea id="{{ $answer->id }}" class="form-control" name="response" rows="4">{{ old('response') }}</textarea>
                                    @endif
                                    <!-- Stores the response for that answer. -->
                                    <input name="answer_id" type="hidden" value="{{ $answer->id }}">
                                @endforeach
                                <div id="clearLeft"></div>
                            @endif
                        </div>
                    </div>
                </label>
            </div>
            <!-- Display the previous question button if the user is not on the first question. -->
            @if ($question->position_number > 1)
                <p id="previousBtn"><a href="/questionnaire/{{ $questionnaire->id }}/step/{{ $question->position_number - 1 }}" class="button">Previous question</a></p>
            @endif
            <!-- Stores the answer(s) for that question. -->
            <input name="question_id" type="hidden" value="{{ $question->id }}">
            <!-- Display the submit questionnaire button if the user is on the last question. -->
            @if ($question->position_number >= count($questionnaire->questions))
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button id="submitBtn" type="submit" class="btn btn-primary">
                            <i class="fa fa-btn"></i>Submit questionnaire
                        </button>
                    </div>
                </div>
            <!-- Display the next question button if the user is not on the last question. -->
            @elseif ($question->position_number + 1)
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button id="nextBtn" type="submit" class="btn btn-primary">
                            <i class="fa fa-btn"></i>Next question
                        </button>
                    </div>
                </div>
            @endif
            <p><a href="/questionnaire/{{ $questionnaire->id }}" class="button alert">Withdraw questionnaire</a></p>
        {!! Form::close() !!}
    </section>
@endsection