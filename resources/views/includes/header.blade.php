<!-- Navigation menu that adapts based on the user browser's/device's screen width. -->
<nav class="top-bar" data-topbar="">
    <ul class="title-area">
        <li class="name">
            <h1><a href="/">Q4U</a></h1>
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
    </ul>
    <section class="top-bar-section">
        <!-- Left-hand navigation menu for displaying links on the left. -->
        <ul class="left">
            <li><a href="/">Home</a></li>
            <li><a href="/browse">Browse</a></li>
            @can('createQuestionnaire')
                <li><a href="/my_questionnaires/create">Create</a></li>
            @endcan
            @can('accessAdminArea')
                <li><a href="/administration">Administration</a></li>
            @endcan
        </ul>
        <!-- Right-hand navigation menu for displaying links on the right. -->
        <ul class="right">
            @if (Auth::user())
                <li class="has-dropdown"><a href="#">{{ $user->email }}</a>
                    <ul class="dropdown">
                        <li><a href="/my_questionnaires">My questionnaires</a></li>
                        <li><a href="/account/{{ $user->id }}">My account</a></li>
                        <li><a href="/logout">Log out</a></li>
                    </ul>
                </li>
            @else
                <li><a href="/register">Register</a></li>
                <li><a href="/login">Log in</a></li>
            @endif
        </ul>
    </section>
</nav>