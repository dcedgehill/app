@extends('layouts.master')
@section('title', 'Questionnaires 4 Us')
@section('content')
    <section class="row large-12 columns">
        <!-- Display login form. -->
        <h1>Log into your account</h1>
        <form id="login" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
            {!! csrf_field() !!}
            <!-- Email field. -->
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">E-Mail Address
                    <div class="col-md-6">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </label>
            </div>
            <!-- Password field. -->
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Password
                    <div class="col-md-6">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <input type="password" class="form-control" name="password">
                    </div>
                </label>
            </div>
            <!-- Allow users to return to the site logged in, even when they close their browsers. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div>
            </div>
            <!-- Submit form to login. -->
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i>Login
                    </button>
                </div>
            </div>
        </form>
    </section>
@endsection
