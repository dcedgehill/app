<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Creates questionnaireresponses table.
 */
class CreateQuestionnaireresponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaireresponses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('researcher_id')->unsigned()->default(0);
            $table->integer('answer_id')->unsigned()->default(0);
            $table->string('response', 200);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answer_question');
    }
}
