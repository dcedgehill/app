<?php

use Illuminate\Database\Seeder;

/**
 * Creates role data.
 */
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['id' => 1, 'name' => "administrator"],
            ['id' => 2, 'name' => "researcher"],
        ]);
    }
}
