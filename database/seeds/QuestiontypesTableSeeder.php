<?php

use Illuminate\Database\Seeder;

/**
 * Creates questiontypes data.
 */
class QuestiontypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questiontypes')->insert([
            ['id' => 1, 'type' => "Closed-ended likert scale"],
            ['id' => 2, 'type' => "Closed-ended net promoter"],
            ['id' => 3, 'type' => "Closed-ended multi-select"],
            ['id' => 4, 'type' => "Open-ended"],
        ]);
    }
}
