<?php

use Illuminate\Database\Seeder;

/**
 * Creates question_questionnaire data.
 */
class QuestionQuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('question_questionnaire')->insert([
            ['question_id' => 1, 'questionnaire_id' => 90],
            ['question_id' => 2, 'questionnaire_id' => 90],
            ['question_id' => 3, 'questionnaire_id' => 90],
            ['question_id' => 4, 'questionnaire_id' => 90],
            ['question_id' => 5, 'questionnaire_id' => 90],
        ]);
    }
}
