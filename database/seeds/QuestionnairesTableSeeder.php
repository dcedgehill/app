<?php

use Illuminate\Database\Seeder;

/**
 * Creates questionnaire data.
 */
class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionnaires')->insert([
            ['id' => 90, 'researcher_id' => 1, 'title' => "Questionnaire design and its use in research", 'description' => "Whenever you create questionnaires, whether they be on paper form or on websites such as SurveyMonkey, you will have noticed that some questions require an answer from a selection of answers, while other questions require an answer from your own choice of words. This is known as questionnaire design. Questionnaire design also takes into account if people prefer to answer closed-ended questions or open-ended questions as well as the length of an average question, closed-ended answers and open-ended responses, and whether people prefer to select one or more answers for a closed-ended question. This questionnaire will provide the opportunity for people who partake in questionnaires to be able to answer the questions and express some of their answers on what they think about questionnaires today.", 'ethical_considerations' => "All data in this questionnaire is anonymous and will only be shared between the questionnaire owner (David Coope), and the Edge Hill University computing department staff. The responses from this questionnaire will be compared with the secondary research to identify whether the Questionnaire 4 Us questionnaire system is suitable for people to partake in questionnaires or not. If you have any concerns regarding the questionnaire or your use of data within the questionnaire, please contact the questionnaire owner (David Coope) at david.coope@go.edgehill.ac.uk.", 'start_date' => "2016-05-07 08:00:00", 'end_date' => "2016-06-09 22:59:00"]
        ]);
    }
}
