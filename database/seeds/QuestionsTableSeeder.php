<?php

use Illuminate\Database\Seeder;

/**
 * Creates question data.
 */
class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['id' => 1, 'type_id' => 3, 'position_number' => 1, 'question' => "Are you aware of the following question types? Please select all that apply."],
            ['id' => 2, 'type_id' => 2, 'position_number' => 2, 'question' => "On a scale of 0-10 (0 = 0 words, 10 = 100 words), how many words do you expect to see in a question?"],
            ['id' => 3, 'type_id' => 2, 'position_number' => 3, 'question' => "On a scale of 0-10 (0 = 0 words, 10 = 10 words), how many words do you expect to see in an answer?"],
            ['id' => 4, 'type_id' => 1, 'position_number' => 4, 'question' => "Would you now consider any of the above questions if you created a questionnaire?"],
            ['id' => 5, 'type_id' => 4, 'position_number' => 5, 'question' => "What is your reasoning for choosing the selected answer from question 4?"],
        ]);
    }
}
