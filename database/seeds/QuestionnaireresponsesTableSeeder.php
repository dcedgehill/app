<?php

use Illuminate\Database\Seeder;

/**
 * Creates questionnaireresponses data.
 */
class QuestionnaireresponsesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionresponses')->insert([
            ['id' => 1, 'researcher_id' => 1, 'answer_id' => 3, 'response' => "Multi-choice"],
            ['id' => 2, 'researcher_id' => 1, 'answer_id' => 4, 'response' => "Open-ended"],
            ['id' => 3, 'researcher_id' => 1, 'answer_id' => 6, 'response' => "1"],
            ['id' => 4, 'researcher_id' => 1, 'answer_id' => 21, 'response' => "5"],
            ['id' => 5, 'researcher_id' => 1, 'answer_id' => 30, 'response' => "Likely"],
            ['id' => 6, 'researcher_id' => 1, 'answer_id' => 32, 'response' => "Because they are very clear in what they are asking you. This means that the questions aren’t confusing so you won’t have any problems answering them."],
            ['id' => 7, 'researcher_id' => 1, 'answer_id' => 1, 'response' => "Likert scale"],
            ['id' => 8, 'researcher_id' => 1, 'answer_id' => 3, 'response' => "Multi-choice"],
            ['id' => 9, 'researcher_id' => 1, 'answer_id' => 4, 'response' => "Open-ended"],
            ['id' => 10, 'researcher_id' => 1, 'answer_id' => 8, 'response' => "3"],
            ['id' => 11, 'researcher_id' => 1, 'answer_id' => 21, 'response' => "5"],
            ['id' => 12, 'researcher_id' => 1, 'answer_id' => 31, 'response' => "Very likely"],
            ['id' => 13, 'researcher_id' => 1, 'answer_id' => 32, 'response' => "These questions allow quantitative data to be collected, which is essential to display the results in relevant graphs."],
            ['id' => 14, 'researcher_id' => 1, 'answer_id' => 3, 'response' => "Multi-choice"],
            ['id' => 15, 'researcher_id' => 1, 'answer_id' => 4, 'response' => "Open-ended"],
            ['id' => 16, 'researcher_id' => 1, 'answer_id' => 6, 'response' => "1"],
            ['id' => 17, 'researcher_id' => 1, 'answer_id' => 26, 'response' => "10"],
            ['id' => 18, 'researcher_id' => 1, 'answer_id' => 29, 'response' => "Undecided"],
            ['id' => 19, 'researcher_id' => 1, 'answer_id' => 32, 'response' => "I'm not sure what sort of questionnaire I am making and so that would determine what type of questions I would use."],
            ['id' => 20, 'researcher_id' => 1, 'answer_id' => 3, 'response' => "Multi-choice"],
            ['id' => 21, 'researcher_id' => 1, 'answer_id' => 4, 'response' => "Open-ended"],
            ['id' => 22, 'researcher_id' => 1, 'answer_id' => 6, 'response' => "1"],
            ['id' => 23, 'researcher_id' => 1, 'answer_id' => 21, 'response' => "5"],
            ['id' => 24, 'researcher_id' => 1, 'answer_id' => 28, 'response' => "Unlikely"],
            ['id' => 25, 'researcher_id' => 1, 'answer_id' => 32, 'response' => "The questions may not be relevant for inclusion in my questionnaire."],
            ['id' => 26, 'researcher_id' => 1, 'answer_id' => 1, 'response' => "Likert scale"],
            ['id' => 27, 'researcher_id' => 1, 'answer_id' => 3, 'response' => "Multi-choice"],
            ['id' => 28, 'researcher_id' => 1, 'answer_id' => 4, 'response' => "Open-ended"],
            ['id' => 29, 'researcher_id' => 1, 'answer_id' => 7, 'response' => "2"],
            ['id' => 30, 'researcher_id' => 1, 'answer_id' => 23, 'response' => "5"],
            ['id' => 31, 'researcher_id' => 1, 'answer_id' => 29, 'response' => "Undecided"],
            ['id' => 32, 'researcher_id' => 1, 'answer_id' => 32, 'response' => "The questions I would use may depend upon the type of questionnaire and subject."],
        ]);
    }
}
