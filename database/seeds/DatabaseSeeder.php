<?php

use Illuminate\Database\Seeder;

/**
 * Execute all seeds at the same time.
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(QuestiontypesTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        $this->call(QuestionnairesTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(QuestionQuestionnaireTableSeeder::class);
        $this->call(AnswersTableSeeder::class);
        $this->call(AnswerQuestionTableSeeder::class);
        $this->call(QuestionnaireresponsesTableSeeder::class);
    }
}
