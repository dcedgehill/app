<?php

use Illuminate\Database\Seeder;

/**
 * Creates permission data.
 */
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            ['id' => 2, 'permission' => "accessAdminArea"],
            ['id' => 1, 'permission' => "createQuestionnaire"],
        ]);
    }
}
