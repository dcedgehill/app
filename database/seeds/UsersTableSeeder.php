<?php

use Illuminate\Database\Seeder;

/**
 * Creates user data.
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            ['id' => 1, 'email' => "admin@example.com", 'password' => '$2y$10$HpTmyHvlmtRaS6zCCJjBPuTMnMf6i/tuold8I2GgnfkXwHmEJhdcS'],
            ['id' => 2, 'email' => "researcher@example.com", 'password' => '$2y$10$l9qXkIF86hIqe3g0Nbl7FOKymyLU4FmwshGqmr8Qa9/6K4oDF9SsW'],
        ]);
    }
}
