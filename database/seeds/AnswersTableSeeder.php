<?php

use Illuminate\Database\Seeder;

/**
 * Creates answer data.
 */
class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            ['id' => 1, 'position_number' => 1, 'answer' => "Likert scale"],
            ['id' => 2, 'position_number' => 2, 'answer' => "Net promoter score"],
            ['id' => 3, 'position_number' => 3, 'answer' => "Multi-choice"],
            ['id' => 4, 'position_number' => 4, 'answer' => "Open-ended"],
            ['id' => 5, 'position_number' => 1, 'answer' => "0"],
            ['id' => 6, 'position_number' => 2, 'answer' => "1"],
            ['id' => 7, 'position_number' => 3, 'answer' => "2"],
            ['id' => 8, 'position_number' => 4, 'answer' => "3"],
            ['id' => 9, 'position_number' => 5, 'answer' => "4"],
            ['id' => 10, 'position_number' => 6, 'answer' => "5"],
            ['id' => 11, 'position_number' => 7, 'answer' => "6"],
            ['id' => 12, 'position_number' => 8, 'answer' => "7"],
            ['id' => 13, 'position_number' => 9, 'answer' => "8"],
            ['id' => 14, 'position_number' => 10, 'answer' => "9"],
            ['id' => 15, 'position_number' => 11, 'answer' => "10"],
            ['id' => 16, 'position_number' => 1, 'answer' => "0"],
            ['id' => 17, 'position_number' => 2, 'answer' => "1"],
            ['id' => 18, 'position_number' => 3, 'answer' => "2"],
            ['id' => 19, 'position_number' => 4, 'answer' => "3"],
            ['id' => 20, 'position_number' => 5, 'answer' => "4"],
            ['id' => 21, 'position_number' => 6, 'answer' => "5"],
            ['id' => 22, 'position_number' => 7, 'answer' => "6"],
            ['id' => 23, 'position_number' => 8, 'answer' => "7"],
            ['id' => 24, 'position_number' => 9, 'answer' => "8"],
            ['id' => 25, 'position_number' => 10, 'answer' => "9"],
            ['id' => 26, 'position_number' => 11, 'answer' => "10"],
            ['id' => 27, 'position_number' => 1, 'answer' => "Very unlikely"],
            ['id' => 28, 'position_number' => 2, 'answer' => "Unlikely"],
            ['id' => 29, 'position_number' => 3, 'answer' => "Undecided"],
            ['id' => 30, 'position_number' => 4, 'answer' => "Likely"],
            ['id' => 31, 'position_number' => 5, 'answer' => "Very likely"],
            ['id' => 32, 'position_number' => 1, 'answer' => "Open-ended"],
        ]);
    }
}
