/**
 * Sets two variables to false to prevent alert windows from appearing when not required.
 */
var submitted = false;
var click = false;

/**
 * Loads the following JavaScript/jQuery code:
 */
$(document).ready(function() {
    /**
     * If the user is currently partaking in a questionnaire and has clicked on a button/link that does submit their
     * current response or takes them back to the previous question, then warn the user that their responses will be
     * removed if they confirm that they want to leave the questionnaire.
     */
    window.onbeforeunload = function() {
        if (window.location.href.indexOf('step') > -1 && !submitted && !click) {
            return "Are you sure you want to withdraw your responses for this questionnaire? Once you confirm, all of your responses for this questionnaire will be removed.";
        }
    }

    /**
     * If the page is able to submit a form, then set the submitted variable value to true.
     *
     * If the page is able to click on a previous button button, then set the submitted variable value to true.
     *
     * If both of these happen, then the system knows that the user is partaking in a questionnaire.
     */
    $("form").submit(function() {
        submitted = true;
    });
    $("#previousBtn").click(function() {
        click = true;
    });

});

/**
 * Confirms the user's action before they proceed to delete the item that they want deleted.
 */
function confirm_delete(action) {
    if (!confirm('Are you sure you want to delete this ' + action + '? Once you confirm, this action cannot be undone.')) {
        return false;
    }
}