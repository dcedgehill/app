<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Provides access to the Questionnaire model and its relations.
 */
class Questionnaire extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'ethical_considerations', 'start_date', 'end_date'
    ];

    /**
     * A questionnaire can have many questions.
     */
    public function questions() {
        return $this->belongsToMany(Question::class);
    }

    /**
     * A questionnaire belongs to one user.
     */
    public function user() {
        return $this->belongsTo(User::class, 'researcher_id');
    }
}
