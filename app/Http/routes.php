<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function () {
    // Allows authentication to occur (for researchers and administrators to log into the system).
    Route::auth();

    // Home page (landing page).
    Route::get('/', 'HomeController@home_index');

    // Administration page for administrative tasks.
    Route::get('/administration', 'HomeController@admin_index');

    // Browse questionnaires page to find relevant questionnaires.
    Route::get('/browse', 'HomeController@browse_index');

    // Search results of questionnaires page to find specific relevant questionnaires.
    Route::get('/browse/results', 'HomeController@search_browse_index');

    // Allows users to be able to swap the positioning of two answers for a question.
    Route::get('/my_questionnaires/{questionnaire_id}/questions/{question_id}/answers/swap_position/{swap}/with/{with}', 'AnswerController@order_answers');

    // Answer pages for creating, reading, updating, and deleting answers.
    Route::resource('/my_questionnaires/{questionnaire_id}/questions/{question_id}/answers', 'AnswerController');

    // Allows users to be able to swap the positioning of two questions for a questionnaire.
    Route::get('/my_questionnaires/{questionnaire_id}/questions/swap_position/{swap}/with/{with}', 'QuestionController@order_questions');

    // Question pages for creating, reading, updating, and deleting questions.
    Route::resource('/my_questionnaires/{id}/questions', 'QuestionController');

    // Results page for specified questionnaire.
    Route::get('/my_questionnaires/{id}/results', 'MyQuestionnaireController@results');

    // Questionnaire pages for creating, reading, updating, and deleting questionnaires.
    Route::resource('/my_questionnaires', 'MyQuestionnaireController');

    // Allows users to be able to confirm whether they want to begin the process of delting their account or not.
    Route::get('/account/{id}/delete', 'AccountController@destroy_confirmation');

    // Account pages for reading, updating, and delete account details.
    Route::resource('/account', 'AccountController');

    // Administration question type pages for creating, reading, updating, and deleting question types.
    Route::resource('/administration/question_types', 'QuestionTypeController');

    // Administration role pages for creating, reading, updating, and deleting roles.
    Route::resource('/administration/roles', 'RoleController');

    // Administration permission pages for creating, reading, updating, and deleting permissions.
    Route::resource('/administration/permissions', 'PermissionController');

    // Administration user pages for reading, assigning roles to users, and deleting users.
    Route::resource('/administration/users', 'UserController');

    // Administration answer pages for reading answers.
    Route::resource('/administration/questionnaires/{questionnaire_id}/questions/{question_id}/answers', 'AdminAnswerController');

    // Administration question pages for reading questions.
    Route::resource('/administration/questionnaires/{id}/questions', 'AdminQuestionController');

    // Administration question pages for reading and deleting questionnaires.
    Route::resource('/administration/questionnaires', 'AdminQuestionnaireController');

    // Allows the user to progress onto the next question of the questionnaire and/or submit their provided responses.
    Route::resource('/questionnaire/{questionnaire_id}/step/{step}/next', 'QuestionnaireController@post_step');

    // Allows the user to answer each of the questions that the questionnaire provides.
    Route::resource('/questionnaire/{questionnaire_id}/step', 'QuestionnaireController@get_step');

    // Questionnaire pages for partaking in questionnaires.
    Route::resource('/questionnaire', 'QuestionnaireController@index');
});