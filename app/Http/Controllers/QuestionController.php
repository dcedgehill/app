<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Question;
use App\Questiontype;
use App\Answer;
use App\Questionnaireresponse;
use Gate;

/**
 * Logged in users can create, read, update, and delete their questions, as well as rearranging the order of them.
 */
class QuestionController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's questions from a specified questionnaire.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            // Selects all questions that return with the user's questions from a specified questionnaire.
            $questions = Question::all();
            return view('my_questionnaires/questions')->with('user', $user)->with('questionnaire', $questionnaire)->with('questions', $questions);
        }
        return redirect('/');
    }

    /**
     * Swaps the positioning of a question with another question
     *
     * @param  int  $id
     * @param  int  $swap
     * @param  int  $with
     * @return \Illuminate\Http\Response
     */
    public function order_questions($id, $swap, $with)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            /**
             * Initialise the following two variables with values of 0.
             *
             * Runs a foreach loop of the questions from the specified questionnaire to identify which question is
             * wanting to swap positions.
             */
            $question_swap = 0;
            $question_swap_with = 0;
            foreach ($questionnaire->questions->sortBy('position_number') as $question) {
                if (Question::where(['id' => $question->id, 'position_number' => $swap])->exists()) {
                    $question_swap = $question->id;
                    break;
                }
            }

            /**
             * Runs a foreach loop of the questions from the specified questionnaire to identify which question is
             * to be swapping positions with what question.
             */
            foreach ($questionnaire->questions->sortBy('position_number') as $question) {
                if (Question::where(['id' => $question->id, 'position_number' => $with])->exists()) {
                    $question_swap_with = $question->id;
                    break;
                }
            }

            /**
             * As long as the two questions exist and are already positioned next to each other to do the swap, begin
             * the processing of swapping the positioning of the two questions.
             */
            if ($question_swap != 0 && $question_swap_with != 0) {
                if ($swap == $with - 1 || $with == $swap - 1) {

                    // Find the two questions and swap their current positions with one another.
                    $question = Question::where('id', $question_swap)->first();
                    $question->position_number = $with;
                    $question->save();
                    $question = Question::where('id', $question_swap_with)->first();
                    $question->position_number = $swap;
                    $question->save();

                    // Question swapping completed.
                    return redirect('/my_questionnaires/' . $id . '/questions#question' . $question_swap);
                }
            }
            return redirect('/my_questionnaires/' . $id . '/questions');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            // Select all question types that the user can select from.
            $questiontypes = Questiontype::all();

            // Select all questions that the user has created from previous existing ones.
            $questions = Question::all()->unique('question');
            return view('my_questionnaires/questions/create')->with('user', $user)->with('questionnaire', $questionnaire)->with('questiontypes', $questiontypes)->with('questions', $questions);
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            /**
             * Validates the user input from the checked fields (question, type_id).
             */
            $this->validate($request, [
                'question' => 'required|max:100',
                'type_id' => 'required|exists:questiontypes,id',
            ]);

            // Store the new question in the database and attach the question type to it.
            $question = Question::create($request->all());
            $question->type_id = $request->type_id;

            /**
             * If the questionnaire has no questions, set the positioning of the new question to 1.
             *
             * If the questionnaire has questions, go through all of the existing questions it has and
             * set the new question right at the end of the last existing question.
             */
            if (count($questionnaire->questions) > 0) {
                foreach ($questionnaire->questions->sortBy('position_number') as $checkQuestion) {
                    if ($checkQuestion->position_number) {
                        $question->position_number = $checkQuestion->position_number + 1;
                    }
                }
            } else {
                $question->position_number = 1;
            }

            /**
             * If the question type is open-ended, automatically create a new answer called Open-ended and
             * attach the question ID to it.
             *
             * This is because open-ended questions should not have pre-defined answers.
             */
            if ($question->type_id == 4) {
                $answer = Answer::create(['position_number' => '1', 'answer' => 'Open-ended']);
                $answer->questions()->attach($question->id);
                $answer->save();
            }

            // Store the new question in the database and attach the questionnaire ID to it.
            $question->questionnaires()->attach($questionnaire->id);
            $question->save();

            // Question created.
            return redirect('/my_questionnaires/' . $id . '/questions');
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's specified question.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::where('id', $question_id)->first();
            return view('my_questionnaires/questions/question')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question);
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function edit($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Users can edit their specified question.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::where('id', $question_id)->first();

            // Select all questions that the user has created from previous existing ones.
            $questions = Question::all();
            return view('my_questionnaires/questions/edit')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)->with('questions', $questions);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            /**
             * Validates the user input from the question field.
             */
            $this->validate($request, [
                'question' => 'required|max:100',
            ]);

            // Find and update the question.
            $question = Question::findOrFail($question_id);
            $question->update($request->all());

            // Question updated.
            return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::find($question_id);

            /**
             * Delete the specified question, as well as its answers and responses.
             *
             * Also ensures that the positioning of the questions are corrected.
             *
             */
            if (count($questionnaire->questions) > 0) {
                foreach ($questionnaire->questions->sortBy('position_number') as $checkQuestion) {
                    if ($question_id == $checkQuestion->id) {
                        $question->delete();
                        $responses = Questionnaireresponse::where('researcher_id', Auth::user()->id);
                        $responses->delete();
                    }
                }
                $questionPosition = 1;
                $questionnaire = Questionnaire::find($questionnaire_id);
                foreach ($questionnaire->questions->sortBy('position_number') as $checkQuestion) {
                    if ($checkQuestion->position_number > $questionPosition) {
                        $updatePosition = Question::where('id', $checkQuestion->id)->first();
                        $updatePosition->position_number = $checkQuestion->position_number - 1;
                        $updatePosition->save();
                    }
                    $questionPosition += 1;
                }
            }

            // Question deleted.
            return redirect('/my_questionnaires/' . $questionnaire_id . '/questions');
        }
        return redirect('/');
    }
}
