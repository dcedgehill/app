<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Permission;
use Gate;

/**
 * Logged in users who are administrators can create, read, update, and delete permissions.
 */
class PermissionController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns permissions and roles.
            $user = User::where('id', Auth::user()->id)->first();
            $roles = Role::all();
            $permissions = Permission::all();
            return view('administration/permissions')->with('user', $user)->with('permissions', $permissions)->with('roles', $roles);
        }
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {
            $user = User::where('id', Auth::user()->id)->first();
            $roles = Role::all();
            return view('administration/permissions/create')->with('user', $user)->with('roles', $roles);
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            /**
             * Validates the user input from the checked fields (permission, role).
             */
            $this->validate($request, [
                'permission' => 'required|max:30|unique:permissions',
                'role' => 'required|exists:roles,id',
            ]);

            // Store the new permission in the database and attach the role ID to it.
            $permission = Permission::create($request->all());
            $permission->roles()->attach($request->input('role'));
            $permission->save();

            // Permission created.
            return redirect('/administration/permissions');
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the specified permission and their roles.
            $user = User::where('id', Auth::user()->id)->first();
            $permission = Permission::where('id', $id)->first();
            $roles = Role::all();
            return view('administration/permissions/permission')->with('user', $user)->with('permission', $permission)->with('roles', $roles);
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // User can edit specified permission.
            $user = User::where('id', Auth::user()->id)->first();
            $permission = Permission::where('id', $id)->first();
            $roles = Role::all();
            return view('administration/permissions/edit')->with('user', $user)->with('permission', $permission)->with('roles', $roles);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            /**
             * Validates the user input from the checked fields (permission, role).
             *
             * If the permission field is equal to the current permission from the database, then ignore the unique rule.
             */
            $permission = permission::findOrFail($id);
            if ($request->permission == $permission->permission) {
                $this->validate($request, [
                    'permission' => 'required|max:30',
                    'role' => 'required|exists:roles,id',
                ]);
            } else {
                $this->validate($request, [
                    'permission' => 'required|max:30|unique:permissions',
                    'role' => 'required',
                ]);
            }

            // Find the permission and update it.
            $permission->update(['permission' => $request->input('permission')]);

            // Ensure the relevant roles contain the up-to-date permissions.
            $roles = $request->get('role');
            $permission->roles()->sync($roles);

            // Permission has been updated.
            return redirect('/administration/permissions/' . $id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Find the permission and delete it.
            $permission = permission::find($id);
            $permission->delete();

            // Permission deleted.
            return redirect('/administration/permissions');
        }
        return redirect('/');
    }
}
