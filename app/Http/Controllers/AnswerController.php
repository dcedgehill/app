<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Question;
use App\Answer;
use App\Questionnaireresponse;
use Gate;

/**
 * Logged in users can create, read, update, and delete their answers, as well as rearranging the order of them.
 */
class AnswerController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's answers from a specified question.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::where('id', $question_id)->first();

            // Selects all answers that return with the user's answers from a specified question.
            $answers = Answer::all();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }
            return view('my_questionnaires/questions/answers')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)->with('answers', $answers);

        }
        return redirect('/');
    }

    /**
     * Swaps the positioning of an answer with another answer
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @param  int  $swap
     * @param  int  $with
     * @return \Illuminate\Http\Response
     */
    public function order_answers($questionnaire_id, $question_id, $swap, $with)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::where('id', $question_id)->first();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }

            /**
             * Initialise the following two variables with values of 0.
             *
             * Runs a foreach loop of the answers from the specified question to identify which answer is
             * wanting to swap positions.
             */
            $answer_swap = 0;
            $answer_swap_with = 0;
            foreach ($question->answers->sortBy('position_number') as $answer) {
                if (Answer::where(['id' => $answer->id, 'position_number' => $swap])->exists()) {
                    $answer_swap = $answer->id;
                    break;
                }
            }

            /**
             * Runs a foreach loop of the answers from the specified question to identify which answer is
             * to be swapping positions with what answer.
             */
            foreach ($question->answers->sortBy('position_number') as $answer) {
                if (Answer::where(['id' => $answer->id, 'position_number' => $with])->exists()) {
                    $answer_swap_with = $answer->id;
                    break;
                }
            }

            /**
             * As long as the two answers exist and are already positioned next to each other to do the swap, begin
             * the processing of swapping the positioning of the two answers.
             */
            if ($answer_swap != 0 && $answer_swap_with != 0) {
                if ($swap == $with - 1 || $with == $swap - 1) {

                    // Find the two answers and swap their current positions with one another.
                    $answer = Answer::where('id', $answer_swap)->first();
                    $answer->position_number = $with;
                    $answer->save();
                    $answer = Answer::where('id', $answer_swap_with)->first();
                    $answer->position_number = $swap;
                    $answer->save();

                    // Answer swapping completed.
                    return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id . '/answers#answer' . $answer_swap);
                }
            }
            return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id . '/answers');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function create($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::where('id', $question_id)->first();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }

            // Select all answers that the user has created from previous existing ones.
            $answers = Answer::all()->unique('answer');
            return view('my_questionnaires/questions/answers/create')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)->with('answers', $answers);
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            /**
             * Validates the user input from the answer field.
             */
            $this->validate($request, [
                'answer' => 'required|max:25',
            ]);

            // Store the new answer in the database and attach the question ID to it.
            $question = Question::where('id', $question_id)->first();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }
            $answer = Answer::create($request->all());

            /**
             * If the question has no answers, set the positioning of the new answer to 1.
             *
             * If the question has answers, go through all of the existing answers it has and
             * set the new answer right at the end of the last existing answer.
             */
            if (count($question->answers) > 0) {
                foreach ($question->answers->sortBy('position_number') as $checkAnswer) {
                    if ($checkAnswer->position_number) {
                        $answer->position_number = $checkAnswer->position_number + 1;
                    }
                }
            } else {
                $answer->position_number = 1;
            }

            // Store the new answer in the database and attach the question ID to it.
            $answer->questions()->attach($question->id);
            $answer->save();

            // Answer created.
            return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id . '/answers');
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @param  int  $answer_id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire_id, $question_id, $answer_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's specified answer.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            $question = Question::where('id', $question_id)->first();
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }
            $answer = Answer::where('id', $answer_id)->first();
            return view('my_questionnaires/questions/answers/answer')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)->with('answer', $answer);
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @param  int  $answer_id
     * @return \Illuminate\Http\Response
     */
    public function edit($questionnaire_id, $question_id, $answer_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Users can edit their specified answer.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::where('id', $question_id)->first();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }
            $answer = Answer::where('id', $answer_id)->first();

            // Select all answers that the user has created from previous existing ones.
            $answers = Answer::all()->unique('answer');
            return view('my_questionnaires/questions/answers/edit')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)->with('answer', $answer)->with('answers', $answers);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $questionnaire_id, $question_id, $answer_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            /**
             * Validates the user input from the answer field.
             */
            $this->validate($request, [
                'answer' => 'required|max:25',
            ]);
            $question = Question::where('id', $question_id)->first();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }

            // Find and update the answer.
            $answer = Answer::findOrFail($answer_id);
            $answer->update($request->all());

            // Answer updated.
            return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id . '/answers/' . $answer_id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @param  int  $answer_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($questionnaire_id, $question_id, $answer_id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            $question = Question::find($question_id);

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }
            $answer = Answer::find($answer_id);

            /**
             * Delete the specified answer, as well as its responses.
             *
             * Also ensures that the positioning of the answers are corrected.
             *
             */
            if (count($question->answers) > 0) {
                foreach ($question->answers->sortBy('position_number') as $checkAnswer) {
                    if ($answer_id == $checkAnswer->id) {
                        $answer->delete();
                        $responses = Questionnaireresponse::where('answer_id', $answer->id);
                        $responses->delete();
                    }
                }
                $answerPosition = 1;
                $question = Question::find($question_id);
                foreach ($question->answers->sortBy('position_number') as $checkAnswer) {
                    if ($checkAnswer->position_number > $answerPosition) {
                        $updatePosition = Answer::where('id', $checkAnswer->id)->first();
                        $updatePosition->position_number = $checkAnswer->position_number - 1;
                        $updatePosition->save();
                    }
                    $answerPosition += 1;
                }
            }

            // Answer deleted.
            return redirect('/my_questionnaires/' . $questionnaire_id . '/questions/' . $question_id . '/answers');
        }
        return redirect('/');
    }
}
