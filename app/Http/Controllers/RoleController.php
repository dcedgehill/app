<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Permission;
use Gate;

/**
 * Logged in users who are administrators can create, read, update, and delete roles,.
 */
class RoleController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns roles and their permissions.
            $user = User::where('id', Auth::user()->id)->first();
            $roles = Role::all();
            $permissions = Permission::all();
            return view('administration/roles')->with('user', $user)->with('roles', $roles)->with('permissions', $permissions);
        }
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('administration/roles/create')->with('user', $user);
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            /**
             * Validates the user input from the name field).
             */
            $this->validate($request, [
                'name' => 'required|max:30|unique:roles',
            ]);

            // Store the new role in the database.
            Role::create($request->all());

            // Role created.
            return redirect('/administration/roles');
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the specified role and their permissions.
            $user = User::where('id', Auth::user()->id)->first();
            $role = Role::where('id', $id)->first();
            $permissions = Permission::all();
            return view('administration/roles/role')->with('user', $user)->with('role', $role)->with('permissions', $permissions);
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // User can edit specified role.
            $user = User::where('id', Auth::user()->id)->first();
            $role = Role::where('id', $id)->first();
            return view('administration/roles/edit')->with('user', $user)->with('role', $role);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            /**
             * Validates the user input from the name field.
             */
            $this->validate($request, [
                'name' => 'required|max:30|unique:roles',
            ]);

            // Find the role and update it.
            $role = role::findOrFail($id);
            $role->update($request->all());

            // Role has been updated.
            return redirect('/administration/roles/' . $id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Find the role and delete it.
            $role = role::find($id);
            $role->delete();

            // Role deleted.
            return redirect('/administration/roles');
        }
        return redirect('/');
    }
}
