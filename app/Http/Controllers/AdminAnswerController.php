<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Question;
use App\Answer;
use Gate;

/**
 * Logged in users who are administrators can read answers.
 */
class AdminAnswerController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int $questionnaire_id
     * @param  int $question_id
     * @return \Illuminate\Http\Response
     */
    public function index($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the answers from a specified question.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();
            $question = Question::where('id', $question_id)->first();
            $answers = Answer::all();

            // Redirect user back to specified question if the user tried to access an open-ended question's answers,
            // as open-ended questions should not have pre-defined answers.
            if ($question->questiontype->type == "Open-ended") {
                return redirect('/administration/questionnaires/' . $questionnaire_id . '/questions/' . $question_id);
            }
            return view('administration/questionnaires/questions/answers')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)->with('answers', $answers);
        }
        return redirect('/');
    }
}