<?php

namespace App\Http\Controllers;

use App\Questionnaireresponse;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Questionnaire;
use App\Question;
use Gate;
use Hash;
use DB;

/**
 * Logged in users can view, update, and delete their own account details.
 */
class AccountController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Redirect user to their own account details.
        return redirect('/account/' . Auth::user()->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Redirect user to their own account details if the supplied user ID does not match with the logged in ID.
        if (Auth::user()->id != $id) {
            return redirect('/account/' . Auth::user()->id);
        }

        // Returns the user's account details.
        $user = User::where('id', Auth::user()->id)->first();
        $roles = Role::all();
        return view('account')->with('user', $user)->with('roles', $roles);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Redirect user to their own account details if the supplied user ID does not match with the logged in ID.
        if (Auth::user()->id != $id) {
            return redirect('account/' . Auth::user()->id);
        }

        // User can edit their account details (e-mail address and password).
        $user = User::where('id', Auth::user()->id)->first();
        return view('account/edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Redirect user to their own account details if the supplied user ID does not match with the logged in ID.
        if (Auth::user()->id != $id) {
            return redirect('account/' . Auth::user()->id);
        }

        // If either of the fields (email, password, account_password) are not blank, then the user is wanting to
        // update at least one of those fields.
        if ($request->email != "" || $request->password != "" || $request->account_password != "") {

            /**
             * Validates the user input from the checked fields (email, password, account_password).
             */
            $this->validate($request, [
                'email' => 'email|max:255|unique:users',
                'password' => 'min:6|max:255',
                'account_password' => 'required|max:255',
            ]);

            // If the user's account password and the password they supplied do not match, then no account details
            // will be updated.
            if (!Hash::check($request->account_password, Auth::user()->password)) {
                $this->validate($request, [
                    'account_password' => 'exists:users,id,password,doesnotmatch',
                ]);
            }

            // Find the user and update their account fields where relevant.
            $user = user::findOrFail($id);
            if ($request->email != "") {
                $user->update(['email' => $request->input('email')]);
            }
            if ($request->password != "") {
                $user->update(['password' => bcrypt($request->input('password'))]);
            }
        }

        // User account details have been updated (e-mail address and/or password).
        return redirect('account/' . Auth::user()->id);
    }

    /**
     * Confirms the specified resource to be removed from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy_confirmation($id)
    {
        // Redirect user to their own account details if the supplied user ID does not match with the logged in ID.
        if (Auth::user()->id != $id) {
            return redirect('account/' . Auth::user()->id);
        }

        // Find the user and confirm their account deletion.
        $user = User::where('id', Auth::user()->id)->first();

        // Grab all users who have an administrator role.
        $usersWithAdminRole = DB::table('role_user')
            ->where('role_id', '1')
            ->get();

        /**
         * If the user who is wanting to delete their account is the only administrator on the system, then they are
         * not able to continue until there is at least two administrators
         */
        foreach ($user->roles as $role) {
            if (count($usersWithAdminRole) < 2 && $role->name == "administrator") {
                return redirect('account/' . Auth::user()->id)->with('statusError', 'Sorry, but the system is unable to allow you to delete your own account due to you being the only administrator. Please assign the administrator role to another existing user.');
            }
        }

        // Confirm account deletion.
        return view('account/delete')->with('user', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // Redirect user to their own account details if the supplied user ID does not match with the logged in ID.
        if (Auth::user()->id != $id) {
            return redirect('account/' . Auth::user()->id);
        }

        /**
         * Validates the user input from the password field.
         */
        $this->validate($request, [
            'password' => 'required|max:255|confirmed',
        ]);

        // If the user's account password and the password they supplied do not match, then no account details
        // will be deleted.
        if (!Hash::check($request->password, Auth::user()->password))
        {
            $this->validate($request, [
                'password' => 'exists:users,id,password,doesnotmatch',
            ]);
        }

        // Find the user and delete their account.
        $user = user::find($id);
        $user->delete();

        // Find the user's questionnaires and delete them.
        $questionnaires = Questionnaire::where('researcher_id', $id);
        $questionnaires->delete();

        // Find the user's questionnaires' responses and delete them.
        $responses = Questionnaireresponse::where('researcher_id', $id);
        $responses->delete();

        // Account deleted.
        // Makes sure that the deleted user is logged out.
        return redirect('/logout');
    }
}
