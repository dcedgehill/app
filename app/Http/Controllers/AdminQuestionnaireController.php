<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Questionnaireresponse;
use Gate;
use DB;

/**
 * Logged in users who are administrators can read and delete questionnaires.
 */
class AdminQuestionnaireController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the questionnaires.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaires = Questionnaire::all();
            return view('administration/questionnaires')->with('user', $user)->with('questionnaires', $questionnaires);
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the specified questionnaire.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();
            return view('administration/questionnaires/questionnaire')->with('user', $user)->with('questionnaire', $questionnaire);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Delete the specified questionnaire.
            $questionnaire = Questionnaire::where('id', $id)->first();
            $questionnaire->delete();

            // Find the questionnaire's responses and delete them.
            $responses = Questionnaireresponse::where('researcher_id', $questionnaire->research_id);
            $responses->delete();

            // Questionnaire deleted.
            return redirect('/administration/questionnaires');
        }
        return redirect('/');
    }
}
