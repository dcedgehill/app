<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Questionnaireresponse;
use Gate;
use DB;

/**
 * Logged in users can create, read, update, and delete their questionnaires.
 */
class MyQuestionnaireController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's questionnaires.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaires = Questionnaire::all();
            return view('my_questionnaires')->with('user', $user)->with('questionnaires', $questionnaires);
        }
        return redirect('/');
    }

    /**
     * Display results for a specified questionnaire.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function results($id) {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's questionnaire results.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();
            $responses = Questionnaireresponse::all();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            return view('my_questionnaires/results')->with('user', $user)->with('questionnaire', $questionnaire)->with('responses', $responses);
        }
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('my_questionnaires/create')->with('user', $user);
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            /**
             * Validates the user input from the checked fields (title, description, ethical_considerations,
             * start_date, end_date).
             */
            $this->validate($request, [
                'title' => 'required|max:200',
                'description' => 'required|max:1000',
                'ethical_considerations' => 'required|max:1000',
                'start_date' => 'required|date|after:' . date("Y-m-d H:i"),
                'end_date' => 'required|date|after:' . date("Y-m-d H:i") . '|after:start_date',
            ]);

            // Store the new questionnaire in the database and attach the logged in user's ID to it.
            $questionnaire = Questionnaire::create($request->all());
            $questionnaire->researcher_id = Auth::user()->id;
            $questionnaire->save();

            // Questionnaire created.
            return redirect('/my_questionnaires');
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Returns the user's specified questionnaire.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            return view('my_questionnaires/questionnaire')->with('user', $user)->with('questionnaire', $questionnaire);
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {

            // Users can edit their specified questionnaire.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }
            return view('my_questionnaires/edit')->with('user', $user)->with('questionnaire', $questionnaire);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            /**
             * Validates the user input from the checked fields (title, description, ethical_considerations,
             * start_date, end_date).
             */
            $this->validate($request, [
                'title' => 'required|max:200',
                'description' => 'required|max:1000',
                'ethical_considerations' => 'required|max:1000',
                'start_date' => 'required|date|after:' . date("Y-m-d H:i"),
                'end_date' => 'required|date|after:' . date("Y-m-d H:i") . '|after:start_date',
            ]);

            // Find and update the questionnaire.
            $questionnaire = questionnaire::findOrFail($id);
            $questionnaire->update($request->all());

            // Questionnaire updated.
            return redirect('/my_questionnaires/' . $id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Allow access if the user requesting to visit this page is a researcher and/or administrator.
        if (Gate::allows('createQuestionnaire')) {
            $questionnaire = Questionnaire::where('id', $id)->first();

            // Redirect user back to their questionnaires if the questionnaire owner ID does not match with the logged in ID.
            if (Auth::user()->id != $questionnaire->researcher_id) {
                return redirect('/my_questionnaires')->with('status', 'Sorry, but you are not authorised to access this questionnaire.');
            }

            // Delete the user's specified questionnaire.
            $questionnaire->delete();

            // Find the user's questionnaire responses and delete them.
            $responses = Questionnaireresponse::where('researcher_id', Auth::user()->id);
            $responses->delete();

            // Questionnaire deleted.
            return redirect('/my_questionnaires');
        }
        return redirect('/');
    }
}
