<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use DB;
use Gate;

/**
 *  Handles the requests for visiting the home, browse, search, and administration home pages.
 */
class HomeController extends Controller
{
    /**
     * Secure the pages from guests except the home, browse, and search pages.
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['home_index', 'browse_index', 'search_browse_index']]);
    }

    /**
     * Display the home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function home_index()
    {
        // Shows relevant links depending on whether the user is logged in.
        if (Auth::user()) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('home')->with('user', $user);
        } else {
            return view('home');
        }
    }

    /**
     * Display the browse page.
     *
     * @return \Illuminate\Http\Response
     */
    public function browse_index()
    {
        // Gets the current date and time and selects all questionnaires between the starting and ending dates.
        $date = date('Y-m-d H:i:s', time());
        $questionnaires = Questionnaire::where('start_date', '<=', $date)->where('end_date', '>=', $date)->get();
        $search_results = "";

        // Shows relevant links depending on whether the user is logged in.
        if (Auth::user()) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('browse')->with('user', $user)->with('search_results', $search_results)->with('questionnaires', $questionnaires);
        } else {
            return view('browse')->with('search_results', $search_results)->with('questionnaires', $questionnaires);
        }
    }

    /**
     * Displays specific searches for finding questionnaires.
     *
     * @return \Illuminate\Http\Response
     */
    public function search_browse_index(Request $request)
    {
        /**
         * Validates the user input from the search_query field.
         */
        $this->validate($request, [
            'search_query' => 'required|max:255',
        ]);

        // Gets the current date and time and selects all questionnaires that contains the key phrases from the
        // search_query field.
        $date = date('Y-m-d H:i:s', time());
        $search_results = Questionnaire::where('title', 'LIKE', '%' . $request->input('search_query') . '%')
            ->orWhere('description', 'LIKE', '%' . $request->input('search_query') . '%')
            ->orWhere('ethical_considerations', 'LIKE', '%' . $request->input('search_query') . '%')
            ->where('start_date', '<=', $date)->where('end_date', '>=', $date)->get();
        $questionnaires = Questionnaire::where('start_date', '<=', $date)->where('end_date', '>=', $date)->get();

        // Shows relevant links depending on whether the user is logged in.
        if (Auth::user()) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('browse')->with('user', $user)->with('search_results', $search_results)->with('questionnaires', $questionnaires);
        } else {
            return view('browse')->with('search_results', $search_results)->with('questionnaires', $questionnaires);
        }
    }

    /**
     * Display the administration home page.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_index()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('administration')->with('user', $user);
        }
        return redirect('/');
    }
}
