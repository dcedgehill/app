<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questiontype;
use Gate;

/**
 * Logged in users who are administrators can create, read, update, and delete question types.
 */
class QuestionTypeController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns question types.
            $user = User::where('id', Auth::user()->id)->first();
            $questiontypes = Questiontype::all();
            return view('administration/question_types')->with('user', $user)->with('questiontypes', $questiontypes);
        }
        return redirect('/');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {
            $user = User::where('id', Auth::user()->id)->first();
            return view('administration/question_types/create')->with('user', $user);
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            /**
             * Validates the user input from the type field.
             */
            $this->validate($request, [
                'type' => 'required|max:100|unique:questiontypes',
            ]);

            // Store the new question type in the database.
            $questiontype = Questiontype::create($request->all());
            $questiontype->save();

            // Question type created.
            return redirect('/administration/question_types');
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the specified question type.
            $user = User::where('id', Auth::user()->id)->first();
            $questiontype = Questiontype::where('id', $id)->first();
            return view('administration/question_types/question_type')->with('user', $user)->with('questiontype', $questiontype);
        }
        return redirect('/');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Edit specified question type.
            $user = User::where('id', Auth::user()->id)->first();
            $questiontype = Questiontype::where('id', $id)->first();
            return view('administration/question_types/edit')->with('user', $user)->with('questiontype', $questiontype);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {
            $questiontype = Questiontype::findOrFail($id);

            /**
             * Validates the user input from the type field.
             *
             * If the type field is equal to the current question type from the database, then ignore the unique rule.
             */
            if ($request->type == $questiontype->type) {
                $this->validate($request, [
                    'type' => 'required|max:100',
                ]);
            } else {
                $this->validate($request, [
                    'type' => 'required|max:100|unique:questiontypes',
                ]);
            }

            // Find and update the question type.
            $questiontype = questiontype::findOrFail($id);
            $questiontype->update($request->all());

            // Question type updated.
            return redirect('/administration/question_types/' . $id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Delete the specified question type.
            $questiontype = questiontype::find($id);
            $questiontype->delete();

            // Question type deleted.
            return redirect('/administration/question_types');
        }
        return redirect('/');
    }
}