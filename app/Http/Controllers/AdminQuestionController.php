<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Question;
use Gate;

/**
 * Logged in users who are administrators can read questions.
 */
class AdminQuestionController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the questions from a specified questionnaire.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $id)->first();
            $questions = Question::all();
            return view('administration/questionnaires/questions')->with('user', $user)->with('questionnaire', $questionnaire)->with('questions', $questions);
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $questionnaire_id
     * @param  int  $question_id
     * @return \Illuminate\Http\Response
     */
    public function show($questionnaire_id, $question_id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns the specified question.
            $user = User::where('id', Auth::user()->id)->first();
            $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();
            $question = Question::where('id', $question_id)->first();
            return view('administration/questionnaires/questions/question')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question);
        }
        return redirect('/');
    }
}
