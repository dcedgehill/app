<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Questionnaire;
use App\Question;
use App\Questiontype;
use App\Answer;
use App\Questionnaireresponse;
use Gate;
use DB;
use Session;

/**
 *  Anyone can visit the questionnaire pages to partake in them.
 */
class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        // Returns the questionnaire that the participant is wanting to partake in.
        $questionnaire = Questionnaire::where('id', $id)->first();

        // Returns all questionnaires on the browse page in case the participant came across one that does not exist.
        $questionnaires = Questionnaire::all();

        // Returns all questions to determine whether the questionnaire has any questions and answers or not.
        $questions = Question::all();

        /**
         * If the questionnaire exists, redirect participant to the specified questionnaire.
         *
         * If the questionnaire does not exist, redirect participant back to the browse page.
         */
        if ($questionnaire) {

            // Shows relevant links depending on whether the user is logged in.
            if (Auth::user()) {
                $user = User::where('id', Auth::user()->id)->first();
                return view('questionnaires/questionnaire')->with('user', $user)->with('questionnaire', $questionnaire)->with('questions', $questions);
            } else {
                return view('questionnaires/questionnaire')->with('questionnaire', $questionnaire)->with('questions', $questions);
            }
        } else {

            // Shows relevant links depending on whether the user is logged in.
            if (Auth::user()) {
                $user = User::where('id', Auth::user()->id)->first();
                return view('browse')->with('user', $user)->with('questionnaires', $questionnaires);
            } else {
                return view('browse')->with('questionnaires', $questionnaires);
            }
        }
    }

    /**
     * Redirects the user to the step that they should be on.
     *
     * @param  int  $questionnaire_id
     * @param  int  $step
     * @return \Illuminate\Http\Response
     */
    public function get_step($questionnaire_id, $step)
    {
        /**
         * Returns the specified questionnaire, along with the start and end timestamps.
         *
         * Initialise the following two variables with values of null.
         */
        $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();
        $questionnaire->start_date = strtotime($questionnaire->start_date);
        $questionnaire->end_date = strtotime($questionnaire->end_date);
        $question_id = null;
        $stepExists = null;

        /**
         * If the questionnaire is still on going and has at least one question, run a foreach loop.
         *
         * The foreach loop loops through the questions from the specified questionnaire to identify which question the
         * participant is supposed to be on, based on the step number.
         *
         * If the found question has at least one answer to it, the question id and step number exist and are not made
         * up.
         */
        if (date('Y-m-d H:i', $questionnaire->start_date) <= date('Y-m-d H:i', time())) {
            if (date('Y-m-d H:i', $questionnaire->end_date) > date('Y-m-d H:i', time())) {
                if (count($questionnaire->questions) > 0) {
                    foreach ($questionnaire->questions->sortBy('position_number') as $question) {
                        if (Question::where(['id' => $question->id, 'position_number' => $step])->exists()) {
                            if (count($question->answers) > 0) {
                                $question_id = $question->id;
                                $stepExists = $step;
                                break;
                            }
                        }
                    }
                }
            }
        }

        /**
         * If the step exists, then return the question based on the question_id variable, the question types to
         * identify what type of question is the question, and the answers belonging to that question.
         */
        if ($stepExists) {
            $question = Question::where('id', $question_id)->first();
            $questiontypes = Questiontype::all();
            $answers = Answer::all();

            /**
             * If the question exists, redirect participant to the specified question.
             *
             * If the question does not exist, redirect participant back to the specified questionnaire.
             */
            if ($question) {

                // Shows relevant links depending on whether the user is logged in.
                if (Auth::user()) {
                    $user = User::where('id', Auth::user()->id)->first();
                    return view('questionnaires/steps/step')->with('user', $user)->with('questionnaire', $questionnaire)->with('question', $question)
                        ->with('questiontypes', $questiontypes)->with('answers', $answers);
                } else {
                    return view('questionnaires/steps/step')->with('questionnaire', $questionnaire)->with('question', $question)
                        ->with('questiontypes', $questiontypes)->with('answers', $answers);
                }
            }
        }
        return redirect('/questionnaire/' . $questionnaire_id);
    }

    /**
     * Redirects the user to the step that they should be on.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $questionnaire_id
     * @param  int  $step
     * @return \Illuminate\Http\Response
     */
    public function post_step(Request $request, $questionnaire_id, $step)
    {
        //Returns the specified question, along with the answers.
        $question = Question::where('id', $request->question_id)->first();
        $answers = Answer::all();

        /**
         * If the question type is open-ended, then the following code occurs.
         *
         * If the question type is closed-ended multi-select, then the following code occurs.
         *
         * If the question type is closed-ended likert scale or net promoter, then the following code occurs.
         */
        if ($question->questiontype->type == "Open-ended") {

            /**
             * Validates the user input from the checked fields (response, answer_id, question_id).
             */
            $this->validate($request, [
                'response' => 'required|max:200',
                'answer_id' => 'exists:answers,id',
                'question_id' => 'exists:questions,id',
            ]);

            /**
             * If a session already exists with the value of the answer ID that the participant had answered, but then
             * changed their mind and provided a different answer, forget the old data for that answer.
             *
             * Create new sessions to store the answer and the answer ID.
             */
            if (Session::get('answer' . $step) == $request->answer_id) {
                Session::forget('answer' . $step, $request->answer_id);
                Session::forget('open-ended' . $step, $request->response);
            }
            Session::put('answer' . $step, $request->answer_id);
            Session::put('open-ended' . $step, $request->response);
        } else if ($question->questiontype->type == "Closed-ended multi-select") {

            /**
             * Validates the user input from the checked fields (response, question_id).
             */
            $this->validate($request, [
                'response' => 'required|exists:answers,id',
                'question_id' => 'exists:questions,id',
            ]);

            /**
             * Initialise the following variable with a value of 0 to begin going through all of the
             * closed-ended answers.
             *
             * Forget the old data for the answers that the user originally selected for the specified step (if the user
             * had to revisit a question again during their time of partaking in the questionnaire).
             *
             * Runs a foreach loop of the answers from the specified question to identify which answers have been
             * checked.
             *
             * If an answer has been checked, create a new session to store the answer ID.
             *
             * If an answer has not been checked, create a new session with no answer (0).
             */
            $indexOfAnswerArray = 0;
            Session::forget('answer' . $step);
            foreach ($question->answers->sortBy('position_number') as $answer) {
                if ($indexOfAnswerArray < count($request->response)) {
                    Session::push('answer' . $step, $request->response[$indexOfAnswerArray]);
                } else {
                    Session::push('answer' . $step, 0);
                }
                $indexOfAnswerArray += 1;
            }
        } else {

            /**
             * Validates the user input from the checked fields (response, question_id).
             */
            $this->validate($request, [
                'response' => 'required|exists:answers,id',
                'question_id' => 'exists:questions,id',
            ]);

            /**
             * Runs a foreach loop of the answers from the specified question to identify whether a session already
             * exists with the value of the answer ID that the participant had answered or not.
             *
             * If true, forget the old data for that answer.
             *
             * Create a new session to store the answer ID.
             */
            foreach ($question->answers->sortBy('position_number') as $answer) {
                if (Session::get('answer' . $step) == $answer->id) {
                    Session::forget('answer' . $step, $answer->id);
                    break;
                }
            }
            Session::put('answer' . $step, $request->response);
        }

        /**
         * Find the specified questionnaire to determine whether the step number has reached the end of
         * the questionnaire.
         *
         * If false, redirect the user to the next question of the questionnaire.
         *
         * If true, begin the process of saving the user's responses.
         */
        $questionnaire = Questionnaire::where('id', $questionnaire_id)->first();
        $step += 1;
        if ($step <= count($questionnaire->questions)) {
            return redirect('/questionnaire/' . $questionnaire_id . '/step/' . $step);
        }
        /**
         * Runs a foreach loop of the questions from the specified questionnaire to identify whether the user has
         * answered all of the questions or not, before saving their responses to the database.
         */
        foreach ($questionnaire->questions->sortBy('position_number') as $question) {
            if (!Session::get('answer' . $question->position_number)) {
                return redirect('/questionnaire/' . $questionnaire_id);
            }

            /**
             * If the question type is open-ended, then save the user's response for the specified question
             * as an open-ended response.
             *
             * If the question type is closed-ended multi-select, then save the user's response for the specified
             * question as a closed-ended multi-select response.
             *
             * If the question type is closed-ended likert scale or net promoter, then save the user's response for the
             * specified question as either one of them (they are dealt exactly the same as they only require one
             * answer).
             */
            if ($question->questiontype->type == "Open-ended") {
                // Find the answer and store the new response in the database and attach the answer ID
                // and researcher ID to it.
                $answer = Answer::where('id', Session::get('answer' . $question->position_number))->first();
                $response = Questionnaireresponse::create(['response' => Session::get('open-ended' . $question->position_number)]);
                $response->researcher_id = $questionnaire->researcher_id;
                $response->answer_id = $answer->id;

                // Response created.
                $response->save();
            } else if ($question->questiontype->type == "Closed-ended multi-select") {
                /**
                 * Runs a foreach loop of the user responses to store them into the database for answers that were
                 * checked. The ones that were not checked will not be stored into the database.
                 */
                foreach (Session::get('answer' . $question->position_number) as $answer) {
                    if ($answer != 0) {
                        // Find the answer and store the new response in the database and attach the answer ID
                        // and researcher ID to it.
                        $answer = Answer::where('id', $answer)->first();
                        $response = Questionnaireresponse::create(['response' => $answer->answer]);
                        $response->researcher_id = $questionnaire->researcher_id;
                        $response->answer_id = $answer->id;

                        // Response created.
                        $response->save();
                    }
                }
            } else {
                // Find the answer and store the new response in the database and attach the answer ID
                // and researcher ID to it.
                $answer = Answer::where('id', Session::get('answer' . $question->position_number))->first();
                $response = Questionnaireresponse::create(['response' => $answer->answer]);
                $response->researcher_id = $questionnaire->researcher_id;;
                $response->answer_id = $answer->id;

                // Response created.
                $response->save();
            }
        }
        return redirect('/questionnaire/' . $questionnaire_id)->with('statusSuccess', 'Your questionnaire response has been submitted successfully. Thank you for partaking.');
    }
}
