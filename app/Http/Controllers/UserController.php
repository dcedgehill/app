<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Questionnaire;
use App\Questionnaireresponse;
use Gate;

/**
 * Logged in users who are administrators can read, assign roles to users, and delete users.
 */
class UserController extends Controller
{
    /**
     * Secure the pages from guests.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Returns users and their roles.
            $user = User::where('id', Auth::user()->id)->first();
            $users = User::all()->except(Auth::user()->id);
            $roles = Role::all();
            return view('administration/users')->with('user', $user)->with('users', $users)->with('roles', $roles);
        }
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Redirect user back to the administration user pages, as they are able to access their own account details
            // on the my account page.
            if (Auth::user()->id == $id) {
                return redirect('administration/users')->with('statusError', 'Sorry, but the system is unable to allow you to read your own account details through the administration area. To read your account details, please go to the my account page.');
            }

            // Returns the specified user and their roles.
            $user = User::where('id', Auth::user()->id)->first();
            $showUser = User::where('id', $id)->first();
            $roles = Role::all();
            return view('administration/users/user')->with('user', $user)->with('showUser', $showUser)->with('roles', $roles);
        }
        return redirect('/');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Redirect user back to the administration user pages, as they are able to access their own account details
            // on the my account page.
            if (Auth::user()->id == $id) {
                return redirect('administration/users')->with('statusError', 'Sorry, but the system is unable to allow you to assign role(s) to your own account. Please contact another administrator if you wish to update your role(s).');
            }

            // User can assign roles to a specified user.
            $user = User::where('id', Auth::user()->id)->first();
            $editUser = User::where('id', $id)->first();
            $roles = Role::all();
            return view('administration/users/edit')->with('user', $user)->with('editUser', $editUser)->with('roles', $roles);
        }
        return redirect('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Redirect user back to the administration user pages, as they are able to access their own account details
            // on the my account page.
            if (Auth::user()->id == $id) {
                return redirect('administration/users')->with('statusError', 'Sorry, but the system is unable to allow you to assign role(s) to your own account. Please contact another administrator if you wish to update your role(s).');
            }

            /**
             * Validates the user input from the role field.
             */
            $this->validate($request, [
                'role' => 'required|exists:roles,id',
            ]);

            // Find the user and update their roles.
            $user = user::findOrFail($id);
            $roles = $request->get('role');
            $user->roles()->sync($roles);

            // User has been updated.
            return redirect('/administration/users/' . $id);
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Allow access if the user requesting to visit this page is an administrator.
        if (Gate::allows('accessAdminArea')) {

            // Redirect user back to the administration user pages, as they are able to access their own account details
            // on the my account page.
            if (Auth::user()->id == $id) {
                return redirect('administration/users/')->with('statusError', 'Sorry, but the system is unable to allow you to delete to your own account through the administration area. To delete your account, please go to the my account page.');
            }

            // Find the user and delete their account.
            $user = user::find($id);
            $user->delete();

            // Find the user's questionnaires and delete them.
            $questionnaires = Questionnaire::where('researcher_id', $id);
            $questionnaires->delete();

            // Find the user's questionnaires' responses and delete them.
            $responses = Questionnaireresponse::where('researcher_id', $id);
            $responses->delete();

            // Account deleted.
            return redirect('/administration/users');
        }
        return redirect('/');
    }
}
