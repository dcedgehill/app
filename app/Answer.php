<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\BrowserKit\Response;

/**
 * Provides access to the Answer model and its relations.
 */
class Answer extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'answer', 'position_number'
    ];

    /**
     * An answer can belong to many questions.
     */
    public function questions() {
        return $this->belongsToMany(Question::class);
    }

    /**
     * An answer has many responses.
     */
    public function responses() {
        return $this->hasMany(Questionnaireresponse::class, 'answer_id');
    }

}
