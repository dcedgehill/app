<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Provides access to the Questiontype model and its relations.
 */
class Questiontype extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
    ];
}
