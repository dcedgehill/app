<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Provides access to the Questionnaireresponse model and its relations.
 */
class Questionnaireresponse extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'response',
    ];
}
