<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Provides access to the Permission model and its relations.
 */
class Permission extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'permission',
    ];

    /**
     * A permission can belong to many roles.
     */
    public function roles() {
        return $this->belongsToMany(Role::class);
    }
}
