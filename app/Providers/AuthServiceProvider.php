<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        /**
         * Register roles and permissions into Laravel authentication system
         */
        foreach($this->getPermissions() as $permission) {
            $gate->define($permission->permission, function($user) use ($permission) {
                return $user->hasRole($permission->roles);
            });
        }
    }

    /**
     * Gets the permissions from the roles
     */
    protected function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
