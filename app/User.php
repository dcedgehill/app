<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Provides access to the User model and its relations.
 */
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user can have many roles.
     */
    public function roles() {
        return $this->belongsToMany(Role::class);
    }

    /**
     * A user can have many questionnaires.
     */
    public function questionnaires() {
        return $this->hasMany(Questionnaire::class, 'researcher_id');
    }

    /**
     * A user can have many responses from  their questionnaires.
     */
    public function questionnaireresponses() {
        return $this->hasMany(Questionnaireresponse::class, 'researcher_id');
    }

    /**
     * Add, edit, or remove a questionnaire from a user.
     */
    public function giveQuestionnaireTo(Questionnaire $questionnaire) {
        return $this->questionnaire()->sync($questionnaire);
    }

    /**
     * Check if a user has been assigned a role.
     */
    public function hasRole($role) {
        if (is_string($role)){
            return $this->roles->contains('name', $role);
        }
        return !! $role->intersect($this->roles)->count();
    }

    /**
     * Assign the user a role.
     */
    public function assignRole($role) {
        return $this->roles()->sync(
            Role::whereName($role)->firstOrFail()
        );
    }
}
