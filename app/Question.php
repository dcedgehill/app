<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Provides access to the Question model and its relations.
 */
class Question extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'type_id', 'position_number'
    ];

    /**
     * A question can belong to many questionnaires.
     */
    public function questionnaires() {
        return $this->belongsToMany(Questionnaire::class);
    }

    /**
     * A question can have one question type.
     */
    public function questiontype() {
        return $this->belongsTo(Questiontype::class, 'type_id');
    }

    /**
     * A question can have many answers.
     */
    public function answers() {
        return $this->belongsToMany(Answer::class);

    }
}
