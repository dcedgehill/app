<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Provides access to the Role model and its relations.
 */
class Role extends Model
{
    // Prevents the laravel default timestamp functions from executing.
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * A role can have many permissions.
     */
    public function permissions() {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Add, edit, or remove a permission from a role.
     */
    public function givePermissionTo(Permission $permission) {
        return $this->permission()->sync($permission);
    }
}
