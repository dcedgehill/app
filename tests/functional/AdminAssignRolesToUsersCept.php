<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Assign roles to users so that administrators have administrator roles and researchers have researcher roles.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Users', 'h3');
$I->see('Read, assign roles to users, and delete users.', 'p');

// Then
$I->click('#users');
$I->amOnPage('/administration/users');
$I->see('Users', 'h1');
$I->see('Read, assign roles to users, and delete users.', 'p');
$I->dontSee('admin@example.com', 'h3');
$I->dontSee('ID: 1', 'li');
$I->see('researcher@example.com', 'h3');
$I->see('ID: 2', 'li');
$I->see('researcher', 'li');
$I->dontSeeRecord('role_user', [
    'role_id' => 1,
    'user_id' => 2,
]);
$I->seeRecord('role_user', [
    'role_id' => 2,
    'user_id' => 2,
]);

// And
$I->click('#user2');
$I->amOnPage('/administration/users/2');
$I->see('researcher@example.com user', 'h1');
$I->see('ID: 2', 'li');
$I->see('researcher', 'li');
$I->dontSee('administrator', 'li');
$I->see('Assign role(s) to user');

// Then
$I->click('Assign role(s) to user');
$I->amOnPage('/administration/users/2/edit');
$I->see('Assign role(s) to researcher@example.com', 'h1');
$I->submitForm('#editUser', [
    '1' => $I->checkOption('#1'),
    '2' => $I->checkOption('#2'),
]);

// And then
$I->seeCurrentUrlEquals('/administration/users/2');
$I->see('researcher@example.com', 'h1');
$I->see('ID: 2', 'li');
$I->see('researcher', 'li');
$I->see('administrator', 'li');
$I->seeRecord('role_user', [
    'role_id' => 1,
    'user_id' => 2,
]);
$I->seeRecord('role_user', [
    'role_id' => 2,
    'user_id' => 2,
]);