<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Create answers so that participants can partake in a questionnaire.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
// Add db test data
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 2,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
// Add questions
$I->haveRecord('questions', [
    'id' => 9998,
    'type_id' => 1,
    'position_number' => 1,
    'question' => 'Question testing',
]);
$I->haveRecord('questions', [
    'id' => 9999,
    'type_id' => 2,
    'position_number' => 2,
    'question' => 'Question Test 2',
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
$I->click('My questionnaires');
$I->see('My questionnaires', 'h1');
$I->see('Questionnaire testing', 'h3');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);

// Then
$I->click('#questionnaire9999');
$I->amOnPage('/my_questionnaires/9999');
$I->see('Questionnaire testing', 'h1');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('View questions and answers');

// And
$I->click('View questions and answers');
$I->amOnPage('/my_questionnaires/9999/questions');
$I->see('Questions and answers - Questionnaire testing', 'h1');
$I->see('#1 Question testing', 'h3');
$I->see('Type: Closed-ended likert scale', 'p');
$I->see('#2 Question Test 2', 'h3');
$I->see('Type: Closed-ended net promoter score', 'p');

// Then
$I->click('#1 Question testing');
$I->amOnPage('/my_questionnaires/9999/questions/9998');
$I->see('#1 Question testing', 'h1');
$I->see('Type: Closed-ended likert scale', 'p');
$I->see('View answers');

// And
$I->click('View answers');
$I->amOnPage('/my_questionnaires/9999/questions/9998/answers');
$I->see('Answers - Question testing', 'h1');
$I->dontSee('#1 Answer test', 'h3');
$I->dontSee('#2 Test', 'h3');
$I->dontSee('#3 Answer test 3', 'h3');
$I->see('Create answer');

// Then
$I->click('Create answer');
$I->amOnPage('/my_questionnaires/9999/questions/9998/answers/create');
$I->see('Create answer', 'h1');
$I->submitForm('#createAnswer', [
    'answer' => 'Answer test',
]);

// And
$I->grabRecord('answer_question', ['question_id' => 9998,]);
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions/9998/answers');
$I->see('Answers - Question testing', 'h1');
$I->see('#1 Answer test', 'h3');
$I->dontSee('#2 Test', 'h3');
$I->dontSee('#3 Answer test 3', 'h3');
$I->see('Create answer');

// Then
$I->click('Create answer');
$I->amOnPage('/my_questionnaires/9999/questions/9998/answers/create');
$I->see('Create answer', 'h1');
$I->submitForm('#createAnswer', [
    'answer' => 'Test',
]);

// And
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions/9998/answers');
$I->see('Answers - Question testing', 'h1');
$I->see('#1 Answer test', 'h3');
$I->see('#2 Test', 'h3');
$I->dontSee('#3 Answer test 3', 'h3');
$I->see('Create answer');

// Then
$I->click('Create answer');
$I->amOnPage('/my_questionnaires/9999/questions/9998/answers/create');
$I->see('Create answer', 'h1');
$I->submitForm('#createAnswer', [
    'answer' => 'Answer test 3',
]);

// And then
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions/9998/answers');
$I->see('Answers - Question testing', 'h1');
$I->see('#1 Answer test', 'h3');
$I->see('#2 Test', 'h3');
$I->see('#3 Answer test 3', 'h3');
$I->see('Create answer');