<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Register an account so that I can log in to CRUD questionnaires.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Register');
$I->amOnPage('/register');
$I->see('Register for an account', 'h1');
$I->submitForm('#register', [
    'email' => 'test@example.com',
    'password' => 'password',
    'password_confirmation' => 'password',
]);

// And
$loggedInUser = $I->grabRecord('users', ['email' => 'test@example.com']);
$I->seeRecord('role_user', ['role_id' => 2, 'user_id' => $loggedInUser->id]);

// Then
$I->seeCurrentUrlEquals('/');
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('test@example.com');
$I->see('Log out');

// And then
$I->click('Log out');
$I->amOnPage('/logout');
$I->seeCurrentUrlEquals('/');
$I->see('Register');
$I->see('Log in');