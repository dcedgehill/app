<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Delete roles so that permissions can no longer be assigned to them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Roles and permissions', 'h3');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');

// Then
// Add db test data
// Add a role called beta tester
$I->haveRecord('roles', [
    'id' => 9999,
    'name' => 'beta tester',
]);
// Add permissions to the beta tester role
$I->haveRecord('permissions', [
    'id' => 9998,
    'permission' => 'testWebsite',
]);
$I->haveRecord('permissions', [
    'id' => 9999,
    'permission' => 'reportTests',
]);
$I->haveRecord('permission_role', [
    'permission_id' => '9998',
    'role_id' => 9999,
]);
$I->haveRecord('permission_role', [
    'permission_id' => 9999,
    'role_id' => 9999,
]);
$I->click('#rolesAndPermissions');
$I->amOnPage('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');
$I->see('Create role');
$I->grabRecord('roles', ['name' => 'beta tester']);
$I->see('beta tester', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 9998,
    'role_id' => 9999,
]);
$I->grabRecord('permission_role', [
    'permission_id' => 9999,
    'role_id' => 9999,
]);
$I->see('testWebsite', 'li');
$I->see('reportTests', 'li');

// And
$I->click('#role9999');
$I->amOnPage('/administration/roles/9999');
$I->see('beta tester', 'h1');
$I->see('testWebsite', 'li');
$I->see('reportTests', 'li');
$I->see('Delete role', 'form');

// Then
$I->submitForm('#deleteRole', []);

// And then
$I->seeCurrentUrlEquals('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');
$I->see('Create role');
$I->dontSee('roles', ['name' => 'beta tester']);
$I->dontSee('beta tester', 'h3');
$I->dontSeeRecord('permission_role', [
    'permission_id' => 9998,
    'role_id' => 9999,
]);
$I->dontSeeRecord('permission_role', [
    'permission_id' => 9999,
    'role_id' => 9999,
]);
$I->dontSee('testWebsite', 'li');
$I->dontSee('reportTests', 'li');