<?php 
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Update my account details so that my account is up-to-date and secure.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
$I->see('My account');
$I->click('My account');
$I->amOnPage('/account/2');
$I->see('My account', 'h1');
$I->see('E-Mail address: researcher@example.com', 'p');
$I->dontSee('administrator');
$I->see('researcher');;
$I->see('Update my account');

// Then
$I->click('Update my account');
$I->amOnPage('/account/2/edit');
$I->see('Update my account', 'h1');
$I->submitForm('#updateMyAccount', [
    'email' => 'researcher2@example.com',
    'password' => 'passcode',
    'account_password' => 'password',
]);

// And then
$I->grabRecord('users', ['email' => 'researcher2@example.com']);
$I->seeCurrentUrlEquals('/account/2');
$I->see('My account', 'h1');
$I->see('E-Mail address: researcher2@example.com', 'p');
$I->dontSee('administrator');
$I->see('researcher');
$I->see('Update my account');