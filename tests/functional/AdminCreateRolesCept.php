<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Create roles so that I can assign permissions to them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Roles and permissions', 'h3');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');

// Then
$I->click('#rolesAndPermissions');
$I->amOnPage('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');
$I->see('Create role');
$I->see('Administrator', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 1
]);
$I->grabRecord('permission_role', [
    'permission_id' => 2,
    'role_id' => 1
]);
$I->see('accessAdminArea', 'li');
$I->see('createQuestionnaire', 'li');
$I->see('Researcher', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 2
]);
$I->see('createQuestionnaire', 'li');
$I->dontSee('Moderator', 'h3');
$I->dontSee('No permissions have been assigned yet to this role.', 'p');

// And
$I->click('Create role');
$I->amOnPage('/administration/roles/create');
$I->see('Create role', 'h1');
$I->submitForm('#createRole', [
    'name' => 'Moderator',
]);

// Then
$I->grabRecord('roles', ['name' => 'Moderator']);
$I->seeCurrentUrlEquals('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Moderator', 'h3');
$I->see('No permissions have been assigned yet to this role.', 'p');