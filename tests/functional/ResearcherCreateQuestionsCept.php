<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Create questions so I can create answers for them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
// Add db test data
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 2,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
$I->click('My questionnaires');
$I->see('My questionnaires', 'h1');
$I->see('Questionnaire testing', 'h3');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);

// Then
$I->click('#questionnaire9999');
$I->amOnPage('/my_questionnaires/9999');
$I->see('Questionnaire testing', 'h1');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('View questions and answers');

// And
$I->click('View questions and answers');
$I->amOnPage('/my_questionnaires/9999/questions');
$I->see('Questions and answers - Questionnaire testing', 'h1');
$I->dontSee('#1 Question testing', 'h3');
$I->dontSee('Type: Closed-ended likert scale', 'p');
$I->dontSee('#2 Question test 2', 'h3');
$I->dontSee('Type: Closed-ended net promoter score', 'p');
$I->dontSee('#3 Question test 3', 'h3');
$I->dontSee('Type: Closed-ended multi-select', 'p');
$I->dontSee('#4 Question test 4', 'h3');
$I->dontSee('Type: Open-ended', 'p');
$I->see('Create question');

// Then
$I->click('Create question');
$I->amOnPage('/my_questionnaires/9999/questions/create');
$I->see('Create question', 'h1');
$I->submitForm('#createQuestion', [
    'question' => 'Question testing',
    'type_id' => '1',
]);

// And
$I->grabRecord('question_questionnaire', ['questionnaire_id' => 9999,]);
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions');
$I->see('Questions and answers - Questionnaire testing', 'h1');
$I->see('#1 Question testing', 'h3');
$I->see('Type: Closed-ended likert scale', 'p');
$I->dontSee('#2 Question test 2', 'h3');
$I->dontSee('Type: Closed-ended net promoter score', 'p');
$I->dontSee('#3 Question test 3', 'h3');
$I->dontSee('Type: Closed-ended multi-select', 'p');
$I->dontSee('#4 Question test 4', 'h3');
$I->dontSee('Type: Open-ended', 'p');
$I->see('Create question');

// Then
$I->click('Create question');
$I->amOnPage('/my_questionnaires/9999/questions/create');
$I->see('Create question', 'h1');
$I->submitForm('#createQuestion', [
    'question' => 'Question test 2',
    'type_id' => '2',
]);

// And
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions');
$I->see('Questions and answers - Questionnaire testing', 'h1');
$I->see('#1 Question testing', 'h3');
$I->see('Type: Closed-ended likert scale', 'p');
$I->see('#2 Question test 2', 'h3');
$I->see('Type: Closed-ended net promoter score', 'p');
$I->dontSee('#3 Question test 3', 'h3');
$I->dontSee('Type: Closed-ended multi-select', 'p');
$I->dontSee('#4 Question test 4', 'h3');
$I->dontSee('Type: Open-ended', 'p');
$I->see('Create question');

// Then
$I->click('Create question');
$I->amOnPage('/my_questionnaires/9999/questions/create');
$I->see('Create question', 'h1');
$I->submitForm('#createQuestion', [
    'question' => 'Question test 3',
    'type_id' => '3',
]);

// And then
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions');
$I->see('Questions and answers - Questionnaire testing', 'h1');
$I->see('#1 Question testing', 'h3');
$I->see('Type: Closed-ended likert scale', 'p');
$I->see('#2 Question test 2', 'h3');
$I->see('Type: Closed-ended net promoter score', 'p');
$I->see('#3 Question test 3', 'h3');
$I->see('Type: Closed-ended multi-select', 'p');
$I->dontSee('#4 Question test 4', 'h3');
$I->dontSee('Type: Open-ended', 'p');
$I->see('Create question');

// Then
$I->click('Create question');
$I->amOnPage('/my_questionnaires/9999/questions/create');
$I->see('Create question', 'h1');
$I->submitForm('#createQuestion', [
    'question' => 'Question test 4',
    'type_id' => '4',
]);

// And then
$I->seeCurrentUrlEquals('/my_questionnaires/9999/questions');
$I->see('Questions and answers - Questionnaire testing', 'h1');
$I->see('#1 Question testing', 'h3');
$I->see('Type: Closed-ended likert scale', 'p');
$I->see('#2 Question test 2', 'h3');
$I->see('Type: Closed-ended net promoter score', 'p');
$I->see('#3 Question test 3', 'h3');
$I->see('Type: Closed-ended multi-select', 'p');
$I->see('#4 Question test 4', 'h3');
$I->see ('Type: Open-ended', 'p');
$I->see('Create question');