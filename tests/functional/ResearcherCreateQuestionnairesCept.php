<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Create questionnaires so I can create questions for them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
$I->click('Create');
$I->amOnPage('/my_questionnaires/create');
$I->see('Create questionnaire', 'h1');
$I->submitForm('#createQuestionnaire', [
    'title' => 'Questionnaire title',
    'description' => 'This questionnaire aims to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous...',
    'start_date' => '2029-06-01 12:30',
    'end_date' => '2029-07-01 17:00',
]);

// And then
$I->seeRecord('questionnaires', ['start_date' => '2029-06-01 12:30', ]);
$I->seeRecord('questionnaires', ['end_date' => '2029-07-01 17:00', ]);
$I->seeCurrentUrlEquals('/my_questionnaires');
$I->see('My questionnaires', 'h1');
$I->see('Questionnaire title', 'h3');
$I->see('Starting date: 2029 June 1st Friday - 12:30 | Ending date: 2029 July 1st Sunday - 17:00', 'p');
$I->see('This questionnaire aims to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous...',
]);