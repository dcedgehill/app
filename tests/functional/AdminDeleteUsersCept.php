<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Delete users, along with their questionnaires, as they are abusing the questionnaire system.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Users', 'h3');
$I->see('Read, assign roles to users, and delete users.', 'p');

// Then
// Add db test data
// Add a new test user
$I->haveRecord('users', [
    'id' => 9999,
    'email' => 'admin2@example.com',
    'password' => 'password',
    'created_at' => '2016-03-31 01:33:15',
    'updated_at' => '2016-03-31 01:33:15',
]);

// Assign the administrator and researcher roles to the new test user
$I->haveRecord('role_user', [
    'role_id' => 1,
    'user_id' => 9999,
]);
$I->haveRecord('role_user', [
    'role_id' => 2,
    'user_id' => 9999,
]);
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 9999,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
// Add questions
$I->haveRecord('questions', [
    'id' => 9998,
    'type_id' => 1,
    'position_number' => 1,
    'question' => 'Question testing',
]);
$I->haveRecord('questions', [
    'id' => 9999,
    'type_id' => 2,
    'position_number' => 2,
    'question' => 'Question Test 2',
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
// Add answers
$I->haveRecord('answers', [
    'id' => 9997,
    'answer' => 'Answer test',
    'position_number' => 1
]);
$I->haveRecord('answers', [
    'id' => 9998,
    'answer' => 'Test',
    'position_number' => 2
]);
$I->haveRecord('answers', [
    'id' => 9999,
    'answer' => 'Answer test 3',
    'position_number' => 3
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9997,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9998,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9999,
    'question_id' => 9998,
]);
// Add responses
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer test',
    'researcher_id' => 9999,
    'answer_id' => 9997,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Test',
    'researcher_id' => 9999,
    'answer_id' => 9998,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer test 3',
    'researcher_id' => 9999,
    'answer_id' => 9999,
]);
$I->click('#users');
$I->amOnPage('/administration/users');
$I->see('Users', 'h1');
$I->see('Read, assign roles to users, and delete users.', 'p');
$I->see('admin2@example.com', 'h3');
$I->see('ID: 9999', 'li');
$I->see('researcher', 'li');
$I->see('administrator', 'li');

// And
$I->click('#user9999');
$I->amOnPage('/administration/users/9999');
$I->see('admin2@example.com user', 'h1');
$I->see('ID: 9999', 'li');
$I->see('researcher', 'li');
$I->see('administrator', 'li');
$I->see('Delete user');

// Then
$I->click('Delete user');
$I->seeCurrentUrlEquals('/administration/users');
$I->dontSeeRecord('users', ['email' => 'admin2@example.com']);
$I->dontSeeRecord('questionnaires', [
    'researcher_id' => 9999,
]);
$I->dontSeeRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->dontSeeRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9997,
    'question_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9998,
    'question_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9999,
    'question_id' => 9999,
]);

$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Answer test',
    'researcher_id' => 9999,
    'answer_id' => 9997,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Test',
    'researcher_id' => 9999,
    'answer_id' => 9998,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Answer test 3',
    'researcher_id' => 9999,
    'answer_id' => 9999,
]);