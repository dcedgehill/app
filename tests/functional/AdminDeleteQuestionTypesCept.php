<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Delete question types so that researchers can no longer use them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Question types', 'h3');
$I->see('Create, read, update, and delete question types.', 'p');

// Then
// Add db test data
// Add a question type called Closed-ended single-select
$I->haveRecord('questiontypes', [
    'id' => 9999,
    'type' => 'Closed-ended single-select',
]);
$I->click('#questionTypes');
$I->amOnPage('/administration/question_types');
$I->see('Question types', 'h1');
$I->see('Create, read, update, and delete question types.', 'p');
$I->see('Create question type');
$I->see('Closed-ended likert scale', 'h3');
$I->see('Closed-ended net promoter score', 'h3');
$I->see('Closed-ended multi-select', 'h3');
$I->see('Open-ended', 'h3');
$I->see('Closed-ended single-select', 'h3');

// And
$I->click('#questionType9999');
$I->amOnPage('/administration/question_types/9999');
$I->see('Closed-ended single-select', 'h1');
$I->see('Delete question type');

// Then
$I->submitForm('#deleteQuestionType', []);

// And
$I->dontSeeRecord('questiontypes', ['type' => 'Closed-ended single-select']);
$I->seeCurrentUrlEquals('/administration/question_types');
$I->see('Question types', 'h1');
$I->see('Create question type');
$I->dontSee('Closed-ended single-select', 'h3');