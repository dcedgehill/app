<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('See questionnaire results so that I can analyse them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
// Add db test data
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 2,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
$I->click('My questionnaires');
$I->see('My questionnaires', 'h1');
$I->see('Questionnaire testing', 'h3');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);

// Add questions, answers, and responses
$I->haveRecord('questions', [
    'id' => 9996,
    'type_id' => 1,
    'position_number' => 1,
    'question' => 'Question testing',
]);
$I->haveRecord('answers', [
    'id' => 9984,
    'position_number' => 1,
    'answer' => 'Answer testing 1',
]);
$I->haveRecord('answers', [
    'id' => 9985,
    'position_number' => 2,
    'answer' => 'Answer Test 2',
]);
$I->haveRecord('answers', [
    'id' => 9986,
    'position_number' => 3,
    'answer' => 'Nothing to add here 3',
]);
$I->haveRecord('answers', [
    'id' => 9987,
    'position_number' => 4,
    'answer' => 'Another answer 4',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9984,
    'question_id' => 9996,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9985,
    'question_id' => 9996,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9986,
    'question_id' => 9996,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9987,
    'question_id' => 9996,
]);

$I->haveRecord('questions', [
    'id' => 9997,
    'type_id' => 2,
    'position_number' => 2,
    'question' => 'Question Test 2',
]);
$I->haveRecord('answers', [
    'id' => 9988,
    'position_number' => 1,
    'answer' => 'Answer testing 5',
]);
$I->haveRecord('answers', [
    'id' => 9989,
    'position_number' => 2,
    'answer' => 'Answer Test 6',
]);
$I->haveRecord('answers', [
    'id' => 9990,
    'position_number' => 3,
    'answer' => 'Nothing to add here 7',
]);
$I->haveRecord('answers', [
    'id' => 9991,
    'position_number' => 4,
    'answer' => 'Another answer 8',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9988,
    'question_id' => 9997,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9989,
    'question_id' => 9997,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9990,
    'question_id' => 9997,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9991,
    'question_id' => 9997,
]);

$I->haveRecord('questions', [
    'id' => 9998,
    'type_id' => 3,
    'position_number' => 3,
    'question' => 'Nothing to add here',
]);
$I->haveRecord('answers', [
    'id' => 9992,
    'position_number' => 1,
    'answer' => 'Answer testing 9',
]);
$I->haveRecord('answers', [
    'id' => 9993,
    'position_number' => 2,
    'answer' => 'Answer Test 10',
]);
$I->haveRecord('answers', [
    'id' => 9994,
    'position_number' => 3,
    'answer' => 'Nothing to add here 11',
]);
$I->haveRecord('answers', [
    'id' => 9995,
    'position_number' => 4,
    'answer' => 'Another answer 12',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9992,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9993,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9994,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9995,
    'question_id' => 9998,
]);

$I->haveRecord('questions', [
    'id' => 9999,
    'type_id' => 4,
    'position_number' => 4,
    'question' => 'Another question',
]);
$I->haveRecord('answers', [
    'id' => 9996,
    'position_number' => 1,
    'answer' => 'Open-ended',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9996,
    'question_id' => 9999,
]);

$I->haveRecord('question_questionnaire', [
    'question_id' => 9996,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9997,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);

$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer testing 1',
    'researcher_id' => 2,
    'answer_id' => 9984,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer Test 6',
    'researcher_id' => 2,
    'answer_id' => 9989,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Nothing to add here 11',
    'answer_id' => 9994,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Nothing to add here 11',
    'researcher_id' => 2,
    'answer_id' => 9994,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'This is an open-ended response.',
    'researcher_id' => 2,
    'answer_id' => 9996,
]);

// Then
$I->click('#questionnaire9999');
$I->amOnPage('/my_questionnaires/9999');
$I->see('Questionnaire testing', 'h1');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('View results');

// And
$I->click('View results');
$I->amOnPage('/my_questionnaires/9999/results');
$I->see('Results - Questionnaire testing', 'h1');
$I->see('#1 Question testing', 'h3');
$I->see('Answer testing 1');
$I->see('1');
$I->see('#2 Question Test 2', 'h3');
$I->see('Answer Test 6');
$I->see('1');
$I->see('#3 Nothing to add here', 'h3');
$I->see('Nothing to add here 11');
$I->see('2');
$I->see('#4 Another question', 'h3');
$I->see('This is an open-ended response.');