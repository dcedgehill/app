<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Create permissions so that I can assign them to roles.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Roles and permissions', 'h3');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');

// Then
$I->click('#rolesAndPermissions');
$I->amOnPage('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');
$I->see('Access list of permissions');
$I->see('Administrator', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 1
]);
$I->grabRecord('permission_role', [
    'permission_id' => 2,
    'role_id' => 1
]);
$I->see('accessAdminArea', 'li');
$I->see('createQuestionnaire', 'li');
$I->see('Researcher', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 2
]);
$I->see('createQuestionnaire', 'li');

// And
$I->click('Access list of permissions');
$I->amOnPage('/administration/permissions');
$I->see('List of permissions', 'h1');
$I->see('Create permission');
$I->see('accessAdminArea', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 2,
    'role_id' => 1
]);
$I->see('administrator', 'li');
$I->see('createQuestionnaire', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 1
]);
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 2
]);
$I->see('administrator', 'li');
$I->see('researcher', 'li');
$I->dontSee('messageUsers');
$I->dontSee('administrator', '#messageUsers ul li');
$I->dontSee('researcher', '#messageUsers ul li');

// Then
$I->click('Create permission');
$I->amOnPage('/administration/permissions/create');
$I->see('Create permission', 'h1');
$I->submitForm('#createPermission', [
    'permission' => 'messageUsers',
    '1' => $I->checkOption('#1'),
    '2' => $I->checkOption('#2'),
]);

// And then
$I->grabRecord('permissions', ['permission' => 'messageUsers']);
$I->seeCurrentUrlEquals('/administration/permissions');
$I->see('List of permissions', 'h1');
$I->see('messageUsers');
$I->see('administrator', '#messageUsers ul li');
$I->see('researcher', '#messageUsers ul li');