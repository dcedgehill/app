<?php 
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('log in so that I can delete users and questionnaires, and assign roles to users.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$loggedInUser = $I->grabRecord('users', ['email' => 'admin@example.com']);
$I->seeRecord('role_user', ['role_id' => 1, 'user_id' => $loggedInUser->id]);
$I->seeRecord('role_user', ['role_id' => 2, 'user_id' => $loggedInUser->id]);

// Then
$I->seeCurrentUrlEquals('/');
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And then
$I->click('Log out');
$I->amOnPage('/logout');
$I->seeCurrentUrlEquals('/');
$I->see('Register');
$I->see('Log in');