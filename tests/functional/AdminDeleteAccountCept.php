<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Delete my account details as I no longer use the questionnaire system for deleting users and questionnaires, and assigning roles to users.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
// Add db test data
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 1,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
// Add questions
$I->haveRecord('questions', [
    'id' => 9998,
    'type_id' => 1,
    'position_number' => 1,
    'question' => 'Question testing',
]);
$I->haveRecord('questions', [
    'id' => 9999,
    'type_id' => 2,
    'position_number' => 2,
    'question' => 'Question Test 2',
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
// Add answers
$I->haveRecord('answers', [
    'id' => 9997,
    'answer' => 'Answer test',
    'position_number' => 1
]);
$I->haveRecord('answers', [
    'id' => 9998,
    'answer' => 'Test',
    'position_number' => 2
]);
$I->haveRecord('answers', [
    'id' => 9999,
    'answer' => 'Answer test 3',
    'position_number' => 3
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9997,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9998,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9999,
    'question_id' => 9998,
]);
// Add responses
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer test',
    'researcher_id' => 1,
    'answer_id' => 9997,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Test',
    'researcher_id' => 1,
    'answer_id' => 9998,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer test 3',
    'researcher_id' => 1,
    'answer_id' => 9999,
]);
$I->see('My account');
$I->click('My account');
$I->amOnPage('/account/1');
$I->see('My account', 'h1');
$I->see('E-Mail address: admin@example.com', 'p');
$I->see('administrator');
$I->see('researcher');;
$I->see('Delete my account');
$I->dontSee('Sorry, but the system is unable to allow you to delete your own account due to you being the only administrator. Please assign the administrator role to another existing user.');

// Then
$I->click('Delete my account');
$I->seeCurrentUrlEquals('/account/1/');
$I->see('Sorry, but the system is unable to allow you to delete your own account due to you being the only administrator. Please assign the administrator role to another existing user.');

// And
// Add a new test user
$I->haveRecord('users', [
    'id' => 9999,
    'email' => 'admin2@example.com',
    'password' => 'password',
]);

// Assign the administrator and researcher roles to the new test user
$I->haveRecord('role_user', [
    'role_id' => 1,
    'user_id' => 9999,
]);
$I->haveRecord('role_user', [
    'role_id' => 2,
    'user_id' => 9999,
]);

// Then
$I->click('Delete my account');
$I->amOnPage('/account/1/delete');
$I->see('Delete my account', 'h1');
$I->submitForm('#deleteMyAccount', [
    'password' => 'password',
    'password_confirmation' => 'password',
]);

// And then
$I->seeCurrentUrlEquals('/');
$I->see('Register');
$I->see('Log in');
$I->dontSeeRecord('users', ['email' => 'admin@example.com']);
$I->dontSeeRecord('questionnaires', [
    'researcher_id' => 1,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);
$I->dontSeeRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->dontSeeRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9997,
    'question_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9998,
    'question_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9999,
    'question_id' => 9999,
]);

$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Answer test',
    'researcher_id' => 1,
    'answer_id' => 9997,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Test',
    'researcher_id' => 1,
    'answer_id' => 9998,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Answer test 3',
    'researcher_id' => 1,
    'answer_id' => 9999,
]);
