<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Delete questionnaires so that the questions for them are also deleted.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
// Add db test data
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 2,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
// Add questions
$I->haveRecord('questions', [
    'id' => 9998,
    'type_id' => 1,
    'position_number' => 1,
    'question' => 'Question testing',
]);
$I->haveRecord('questions', [
    'id' => 9999,
    'type_id' => 2,
    'position_number' => 2,
    'question' => 'Question Test 2',
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
// Add answers
$I->haveRecord('answers', [
    'id' => 9997,
    'answer' => 'Answer test',
    'position_number' => 1
]);
$I->haveRecord('answers', [
    'id' => 9998,
    'answer' => 'Test',
    'position_number' => 2
]);
$I->haveRecord('answers', [
    'id' => 9999,
    'answer' => 'Answer test 3',
    'position_number' => 3
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9997,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9998,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9999,
    'question_id' => 9998,
]);
// Add responses
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer test',
    'researcher_id' => 2,
    'answer_id' => 9997,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Test',
    'researcher_id' => 2,
    'answer_id' => 9998,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer test 3',
    'researcher_id' => 2,
    'answer_id' => 9999,
]);
$I->click('My questionnaires');
$I->see('My questionnaires', 'h1');
$I->see('Questionnaire testing', 'h3');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', ]);

// Then
$I->click('#questionnaire9999');
$I->amOnPage('/my_questionnaires/9999');
$I->see('Questionnaire testing', 'h1');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('Delete questionnaire');

// And
$I->submitForm('#deleteQuestionnaire', []);

// And then
$I->seeCurrentUrlEquals('/my_questionnaires');
$I->see('My questionnaires', 'h1');
$I->dontSee('Questionnaire testing', 'h3');
$I->dontSee('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->dontSee('This questionnaire will aim to test the questionnaire system...', 'p');
$I->dontSeeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);
$I->dontSeeRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->dontSeeRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9997,
    'question_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9998,
    'question_id' => 9999,
]);
$I->dontSeeRecord('answer_question', [
    'answer_id' => 9999,
    'question_id' => 9999,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Answer test',
    'researcher_id' => 2,
    'answer_id' => 9997,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Test',
    'researcher_id' => 2,
    'answer_id' => 9998,
]);
$I->dontSeeRecord('questionnaireresponses', [
    'response' => 'Answer test 3',
    'researcher_id' => 2,
    'answer_id' => 9999,
]);