<?php
$I = new FunctionalTester($scenario);

$I->am('participant');
$I->wantTo('Partake in questionnaires so that I can provide my views about the questionnaire topic.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');
$I->see('Browse');

// Then
// Add db test data
// Add a questionnaire
$I->haveRecord('questionnaires', [
    'id' => 9998,
    'researcher_id' => 1,
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2016-04-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 2,
    'title' => 'About questionnaires',
    'description' => 'This second questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this second questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2016-04-04 11:00',
    'end_date' => '2029-07-03 20:00',
]);
$I->click('Browse');
$I->amOnPage('/browse');
$I->see('Browse questionnaires', 'h1');
$I->see('Recently available', 'h3');
$I->dontSee('Search "second questionnaire" | Returned (1) search result', 'h3');
$I->see('Questionnaire testing', 'h3');
$I->see('Starting date: 2016 April 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 1,
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);
$I->see('About questionnaires', 'h3');
$I->see('Starting date: 2016 April 4th Monday - 11:00 | Ending date: 2029 July 3rd Tuesday - 20:00', 'p');
$I->see('This second questionnaire will aim to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this second questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
]);

// And
// Add db test data
// Add questions and answers
$I->haveRecord('questions', [
    'id' => 9996,
    'type_id' => 1,
    'position_number' => 1,
    'question' => 'Question testing',
]);
$I->haveRecord('answers', [
    'id' => 9984,
    'position_number' => 1,
    'answer' => 'Answer testing 1',
]);
$I->haveRecord('answers', [
    'id' => 9985,
    'position_number' => 2,
    'answer' => 'Answer Test 2',
]);
$I->haveRecord('answers', [
    'id' => 9986,
    'position_number' => 3,
    'answer' => 'Nothing to add here 3',
]);
$I->haveRecord('answers', [
    'id' => 9987,
    'position_number' => 4,
    'answer' => 'Another answer 4',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9984,
    'question_id' => 9996,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9985,
    'question_id' => 9996,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9986,
    'question_id' => 9996,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9987,
    'question_id' => 9996,
]);

$I->haveRecord('questions', [
    'id' => 9997,
    'type_id' => 2,
    'position_number' => 2,
    'question' => 'Question Test 2',
]);
$I->haveRecord('answers', [
    'id' => 9988,
    'position_number' => 1,
    'answer' => 'Answer testing 5',
]);
$I->haveRecord('answers', [
    'id' => 9989,
    'position_number' => 2,
    'answer' => 'Answer Test 6',
]);
$I->haveRecord('answers', [
    'id' => 9990,
    'position_number' => 3,
    'answer' => 'Nothing to add here 7',
]);
$I->haveRecord('answers', [
    'id' => 9991,
    'position_number' => 4,
    'answer' => 'Another answer 8',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9988,
    'question_id' => 9997,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9989,
    'question_id' => 9997,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9990,
    'question_id' => 9997,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9991,
    'question_id' => 9997,
]);

$I->haveRecord('questions', [
    'id' => 9998,
    'type_id' => 3,
    'position_number' => 3,
    'question' => 'Nothing to add here',
]);
$I->haveRecord('answers', [
    'id' => 9992,
    'position_number' => 1,
    'answer' => 'Answer testing 9',
]);
$I->haveRecord('answers', [
    'id' => 9993,
    'position_number' => 2,
    'answer' => 'Answer Test 10',
]);
$I->haveRecord('answers', [
    'id' => 9994,
    'position_number' => 3,
    'answer' => 'Nothing to add here 11',
]);
$I->haveRecord('answers', [
    'id' => 9995,
    'position_number' => 4,
    'answer' => 'Another answer 12',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9992,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9993,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9994,
    'question_id' => 9998,
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9995,
    'question_id' => 9998,
]);

$I->haveRecord('questions', [
    'id' => 9999,
    'type_id' => 4,
    'position_number' => 4,
    'question' => 'Another question',
]);
$I->haveRecord('answers', [
    'id' => 9996,
    'position_number' => 1,
    'answer' => 'Open-ended',
]);
$I->haveRecord('answer_question', [
    'answer_id' => 9996,
    'question_id' => 9999,
]);

$I->haveRecord('question_questionnaire', [
    'question_id' => 9996,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9997,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9998,
    'questionnaire_id' => 9999,
]);
$I->haveRecord('question_questionnaire', [
    'question_id' => 9999,
    'questionnaire_id' => 9999,
]);

$I->click('#questionnaire9999');
$I->amOnPage('/questionnaire/9999');
$I->dontSee('Your questionnaire response has been submitted successfully. Thank you for partaking.');
$I->see('About questionnaires', 'h1');
$I->see('Starting date: 2016 April 4th Monday - 11:00 | Ending date: 2029 July 3rd Tuesday - 20:00', 'p');
$I->see('This second questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this second questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('Partake in questionnaire');

// Then
$I->click('Partake in questionnaire');
$I->seeCurrentUrlEquals('/questionnaire/9999/step/1');
$I->see('#1 Question testing', 'h1');
$I->see('Type: Closed-ended likert scale');
$I->see('Answer testing 1');
$I->see('Answer Test 2');
$I->see('Nothing to add here 3');
$I->see('Another answer 4');
$I->dontSee('Previous question', '#previousBtn');
$I->see('Next question', '#nextBtn');
$I->dontSee('Complete questionnaire');
$I->see('Withdraw questionnaire');

// And
$I->submitForm('#submitResponse', [
    '9984' => $I->selectOption('form input[name=response]', '9984'),
]);
$I->seeInSession('answer1', '9984');
$I->seeCurrentUrlEquals('/questionnaire/9999/step/2');
$I->see('#2 Question Test 2', 'h1');
$I->see('Type: Closed-ended net promoter score');
$I->see('Answer testing 5');
$I->see('Answer Test 6');
$I->see('Nothing to add here 7');
$I->see('Another answer 8');
$I->see('Previous question', '#previousBtn');
$I->see('Next question', '#nextBtn');
$I->dontSee('Complete questionnaire');
$I->see('Withdraw questionnaire');

// Then
$I->submitForm('#submitResponse', [
    '9989' => $I->selectOption('form input[name=response]', '9989'),
]);
$I->seeInSession('answer2', '9989');
$I->seeCurrentUrlEquals('/questionnaire/9999/step/3');
$I->see('#3 Nothing to add here', 'h1');
$I->see('Type: Closed-ended multi-select');
$I->see('Answer testing 9');
$I->see('Answer Test 10');
$I->see('Nothing to add here 11');
$I->see('Another answer 12');
$I->see('Previous question', '#previousBtn');
$I->see('Next question', '#nextBtn');
$I->dontSee('Complete questionnaire');
$I->see('Withdraw questionnaire');

// And
$I->submitForm('#submitResponse', [
    '9994' => $I->checkOption('#9994'),
    '9995' => $I->checkOption('#9995'),
]);
$I->seeInSession('answer3');
$I->seeCurrentUrlEquals('/questionnaire/9999/step/4');
$I->see('#4 Another question', 'h1');
$I->see('Type: Open-ended');
$I->see('Open-ended');
$I->see('Previous question', '#previousBtn');
$I->dontSee('Next question', '#nextBtn');
$I->see('Submit questionnaire');
$I->see('Withdraw questionnaire');

// Then
$I->submitForm('#submitResponse', [
    '9996' => 'This is an open-ended response.',
]);
$I->amOnPage('/questionnaire/9999');
$I->see('About questionnaires', 'h1');
$I->see('Starting date: 2016 April 4th Monday - 11:00 | Ending date: 2029 July 3rd Tuesday - 20:00', 'p');
$I->see('This second questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this second questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('Partake in questionnaire');
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer testing 1',
    'researcher_id' => 2,
    'answer_id' => 9984,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Answer Test 6',
    'researcher_id' => 2,
    'answer_id' => 9989,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Nothing to add here 11',
    'answer_id' => 9994,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'Nothing to add here 11',
    'researcher_id' => 2,
    'answer_id' => 9994,
]);
$I->haveRecord('questionnaireresponses', [
    'response' => 'This is an open-ended response.',
    'researcher_id' => 2,
    'answer_id' => 9996,
]);