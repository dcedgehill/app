<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Delete permissions so that the roles do not contain unwanted permissions.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Roles and permissions', 'h3');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');

// Then
$I->click('#rolesAndPermissions');
$I->amOnPage('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');
$I->see('Access list of permissions');
$I->see('Administrator', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 1
]);
$I->grabRecord('permission_role', [
    'permission_id' => 2,
    'role_id' => 1
]);
$I->see('accessAdminArea', 'li');
$I->see('createQuestionnaire', 'li');
$I->see('Researcher', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 1,
    'role_id' => 2
]);
$I->see('createQuestionnaire', 'li');

// And
// Add db test data
// Add a permission called userContact
$I->haveRecord('permissions', [
    'id' => '9999',
    'permission' => 'userContact',
]);
$I->haveRecord('permission_role', [
    'permission_id' => '9999',
    'role_id' => '1',
]);
$I->click('Access list of permissions');
$I->amOnPage('/administration/permissions');
$I->see('List of permissions', 'h1');
$I->see('Create permission');
$I->see('userContact', 'h3');
$I->grabRecord('permission_role', [
    'permission_id' => 9999,
    'role_id' => 1
]);
$I->see('administrator', '#userContact ul li');

// Then
$I->click('#permission9999');
$I->amOnPage('/administration/permissions/9999');
$I->see('userContact', 'h1');
$I->see('administrator', 'li');
$I->see('Delete permission');

// And
$I->submitForm('#deletePermission', []);

// Then
$I->seeCurrentUrlEquals('/administration/permissions');
$I->see('List of permissions', 'h1');
$I->see('Create permission');
$I->dontSee('userContact', 'h3');
$I->dontSeeRecord('permission_role', ['permission_id' => 9999]);
$I->dontSee('administrator', '#userContact ul li');