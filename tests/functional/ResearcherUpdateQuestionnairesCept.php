<?php
$I = new FunctionalTester($scenario);

$I->am('researcher');
$I->wantTo('Update questionnaires so I can keep the questionnaires up-to-date.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'researcher@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'researcher@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->dontSee('Administration');
$I->see('researcher@example.com');
$I->see('Log out');

// And
// Add db test data
// Add a role called tester
$I->haveRecord('questionnaires', [
    'id' => 9999,
    'researcher_id' => 2,
    'title' => 'Questionnaire title',
    'description' => 'This questionnaire aims to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous...',
    'start_date' => '2029-06-01 12:30',
    'end_date' => '2029-07-01 17:00',
]);
$I->amOnPage('/my_questionnaires');
$I->see('My questionnaires', 'h1');
$I->see('Questionnaire title', 'h3');
$I->see('Starting date: 2029 June 1st Friday - 12:30 | Ending date: 2029 July 1st Sunday - 17:00', 'p');
$I->see('This questionnaire aims to test the questionnaire system...', 'p');
$I->seeRecord('questionnaires', [
    'researcher_id' => 2,
    'ethical_considerations' => 'All data in this questionnaire is anonymous...',
]);

// Then
$I->click('#questionnaire9999');
$I->amOnPage('/my_questionnaires/9999');
$I->see('Questionnaire title', 'h1');
$I->see('Starting date: 2029 June 1st Friday - 12:30 | Ending date: 2029 July 1st Sunday - 17:00', 'p');
$I->see('This questionnaire aims to test the questionnaire system...', 'p');
$I->see('All data in this questionnaire is anonymous...', 'p');
$I->see('Edit questionnaire');

// And
$I->click('Edit questionnaire');
$I->amOnPage('/my_questionnaires/9999/edit');
$I->see('Edit Questionnaire title questionnaire', 'h1');
$I->submitForm('#editQuestionnaire', [
    'title' => 'Questionnaire testing',
    'description' => 'This questionnaire will aim to test the questionnaire system...',
    'ethical_considerations' => 'All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...',
    'start_date' => '2029-06-04 11:00',
    'end_date' => '2029-07-02 20:00',
]);

// Then
$I->seeRecord('questionnaires', ['start_date' => '2029-06-04 11:00', ]);
$I->seeRecord('questionnaires', ['end_date' => '2029-07-02 20:00', ]);
$I->seeCurrentUrlEquals('/my_questionnaires/9999');
$I->see('Questionnaire testing', 'h1');
$I->see('Starting date: 2029 June 4th Monday - 11:00 | Ending date: 2029 July 2nd Monday - 20:00', 'p');
$I->see('This questionnaire will aim to test the questionnaire system...', 'p');
$I->see('All data in this questionnaire is anonymous. Please contact the team at test@example.com for any queries/questions...', 'p');
$I->see('Edit questionnaire');