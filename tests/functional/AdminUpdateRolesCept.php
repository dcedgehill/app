<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Update roles so that I can assign permissions to them.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Roles and permissions', 'h3');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');

// Then
// Add db test data
// Add a role called tester
$I->haveRecord('roles', [
    'id' => 9999,
    'name' => 'tester',
]);
$I->click('#rolesAndPermissions');
$I->amOnPage('/administration/roles');
$I->see('Roles and permissions', 'h1');
$I->see('Create, read, update, and delete roles, as well as for permissions belonging to existing roles.', 'p');
$I->see('Create role');
$I->grabRecord('roles', ['name' => 'tester']);
$I->see('tester', 'h3');
$I->see('No permissions have been assigned yet to this role.', 'p');

// And
$I->click('#role9999');
$I->amOnPage('/administration/roles/9999');
$I->see('tester', 'h1');
$I->see('No permissions have been assigned yet to this role.', 'p');
$I->see('Edit role');

// Then
$I->click('Edit role');
$I->amOnPage('/administration/roles/9999/edit');
$I->see('Edit tester role', 'h1');
$I->submitForm('#editRole', [
    'name' => 'beta tester',
]);

// And then
$I->grabRecord('roles', ['name' => 'beta tester']);
$I->seeCurrentUrlEquals('/administration/roles/9999');
$I->see('beta tester', 'h1');
$I->see('No permissions have been assigned yet to this role.', 'p');