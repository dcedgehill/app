<?php
$I = new FunctionalTester($scenario);

$I->am('administrator');
$I->wantTo('Update question types so that they are up-to-date.');

// When
$I->amOnPage('/');
$I->see('Register');
$I->see('Log in');
$I->dontSee('Create');
$I->dontSee('Administration');

// Then
$I->click('Log in');
$I->amOnPage('/login');
$I->see('Log into your account', 'h1');
$I->submitForm('#login', [
    'email' => 'admin@example.com',
    'password' => 'password',
]);

// And
$I->grabRecord('users', ['email' => 'admin@example.com']);

// Then
$I->dontSee('Register');
$I->dontSee('Log in');
$I->see('Create');
$I->see('Administration');
$I->see('admin@example.com');
$I->see('Log out');

// And
$I->click('Administration');
$I->amOnPage('/administration');
$I->see('Administration', 'h1');
$I->see('Question types', 'h3');
$I->see('Create, read, update, and delete question types.', 'p');

// Then
// Add db test data
// Add a question type called Closed-ended single-select
$I->haveRecord('questiontypes', [
    'id' => 9999,
    'type' => 'Closed-ended single-select',
]);
$I->click('#questionTypes');
$I->amOnPage('/administration/question_types');
$I->see('Question types', 'h1');
$I->see('Create, read, update, and delete question types.', 'p');
$I->see('Create question type');
$I->see('Closed-ended likert scale', 'h3');
$I->see('Closed-ended net promoter score', 'h3');
$I->see('Closed-ended multi-select', 'h3');
$I->see('Open-ended', 'h3');
$I->see('Closed-ended single-select', 'h3');

// And
$I->click('#questionType9999');
$I->amOnPage('/administration/question_types/9999');
$I->see('Closed-ended single-select', 'h1');
$I->see('Edit question type');

// Then
$I->click('Edit question type');
$I->amOnPage('/administration/question_types/9999/edit');
$I->see('Edit Closed-ended single-select question type', 'h1');
$I->submitForm('#editQuestionType', [
    'type' => 'Open-ended large-box',
]);

// And
$I->grabRecord('questiontypes', ['type' => 'Open-ended large-box']);
$I->seeCurrentUrlEquals('/administration/question_types/9999');
$I->see('Open-ended large-box', 'h1');