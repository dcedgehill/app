# Questionnaire System - Readme file

PLEASE NOTE THAT GULP MIGHT HAVE TO BE INSTALLED, ESPECIALLY IF YOU PLAN TO MODIFY THE APPLICATION.

Once the repository has been downloaded through the git clone https://bitbucket.org/davidcoope/app bash command, run the composer update bash command inside the repository folder (app). If you get an error that the questionnairesystem database is unknown, do not worry. This will be resolved in the next lot of steps.

Open up the MySQL Workbench application on your Linux machine.

In the MySQL Connections section, left click on the Local instance 3306 box. If the number is not 3306 but still says Local instance, left click on it anyway.

A prompt window will appear, asking for the password to the root user. Type "webdev" (without the double quotes) as the password and left click the OK button to proceed.

On the left hand side of the MySQL Workbench window, there is a section called MANAGEMENT. Within that section, left click on the Data Import/Restore link and type the same password again ("webdev") (without the double quotes) for the root user and left click the OK button to proceed.

In the Import from Disk section (assuming the tab for it is already highlighted), left click on the Import from Self-Contained File radio button, left click on the ... button on the far right of the Import from Self-Contained File radio button, and then select the questionnairesystem.sql file inside of the app folder (application you downloaded) and load that file into the MySQL Work bench application.

At the bottom of the MySQL Workbench window, left click on the Start Import button and type the same password again ("webdev") (without the double quotes) for the root user to begin importing the questionnairesystem.sql file into the application.

Ensure the Laravel project works by running the php artisan serve command in the Laravel project directory (app).

Open up localhost:8000 on a browser such as Chrome or Firefox. If the home/landing page shows a heading saying Welcome to Questionnaires 4 Us, then you are now ready to use the system to create questionnaires and do administrative tasks.

# Notes

Questionnaire login details (localhost:8000/login):

Administrator account:

Email: admin@example.com 

Password: password

Researcher account: 

Email: researcher@example.com 

Password: password

BDD (functional) tests:

To test that all of the application features work, run the php vendor/bin/codecept run functional bash command inside the app Laravel project. 

PLEASE NOTE: Before running this bash command, in the resources/views/layouts/master.blade.php file, remove all code that is wrapped in php tags or otherwise errors will return when trying to run all of the functional tests.

Once you have done the testing, please revert the resources/views/layouts/master.blade.php file back to its original state.
