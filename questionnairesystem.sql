CREATE DATABASE  IF NOT EXISTS `questionnairesystem` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `questionnairesystem`;
-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: questionnairesystem
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer_question`
--

DROP TABLE IF EXISTS `answer_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_question` (
  `answer_id` int(10) unsigned NOT NULL,
  `question_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`answer_id`,`question_id`),
  KEY `answer_question_question_id_foreign` (`question_id`),
  CONSTRAINT `answer_question_answer_id_foreign` FOREIGN KEY (`answer_id`) REFERENCES `answers` (`id`) ON DELETE CASCADE,
  CONSTRAINT `answer_question_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_question`
--

LOCK TABLES `answer_question` WRITE;
/*!40000 ALTER TABLE `answer_question` DISABLE KEYS */;
INSERT INTO `answer_question` VALUES (1,1),(2,1),(3,1),(4,1),(10000,2),(10001,2),(10002,2),(10003,2),(10004,2),(10005,2),(10006,2),(10007,2),(10008,2),(10009,2),(10010,2),(10011,3),(10012,3),(10013,3),(10014,3),(10015,3),(10016,3),(10017,3),(10018,3),(10019,3),(10020,3),(10021,3),(10022,4),(10023,4),(10024,4),(10025,4),(10026,4),(7,5),(5,6),(6,6),(10033,10010);
/*!40000 ALTER TABLE `answer_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position_number` int(10) NOT NULL,
  `answer` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10034 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,1,'Likert scale'),(2,2,'Net promoter score'),(3,3,'Multi-choice'),(4,4,'Open-ended'),(5,1,'Pizza'),(6,2,'Burger'),(7,1,'Open-ended'),(10000,1,'0'),(10001,2,'1'),(10002,3,'2'),(10003,4,'3'),(10004,5,'4'),(10005,6,'5'),(10006,7,'6'),(10007,8,'7'),(10008,9,'8'),(10009,10,'9'),(10010,11,'10'),(10011,1,'0'),(10012,2,'1'),(10013,3,'2'),(10014,4,'3'),(10015,5,'4'),(10016,6,'5'),(10017,7,'6'),(10018,8,'7'),(10019,9,'8'),(10020,10,'9'),(10021,11,'10'),(10022,1,'Very unlikely'),(10023,2,'Unlikely'),(10024,3,'Undecided'),(10025,4,'Likely'),(10026,5,'Very likely'),(10033,1,'Open-ended');
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_03_29_143707_create_roles_table',1),('2016_03_29_143708_create_role_user_table',1),('2016_03_29_143716_create_permissions_table',1),('2016_03_29_145400_create_permission_role_table',1),('2016_03_29_145401_create_questionnaires_table',1),('2016_03_29_145402_create_questions_table',1),('2016_03_29_145433_create_question_questionnaire_table',1),('2016_03_29_145441_create_answers_table',1),('2016_03_29_145442_create_answer_question_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(1,2);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_permission_unique` (`permission`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (2,'accessAdminArea'),(1,'createQuestionnaire');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question_questionnaire`
--

DROP TABLE IF EXISTS `question_questionnaire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question_questionnaire` (
  `question_id` int(10) unsigned NOT NULL,
  `questionnaire_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`question_id`,`questionnaire_id`),
  KEY `question_questionnaire_questionnaire_id_foreign` (`questionnaire_id`),
  CONSTRAINT `question_questionnaire_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `question_questionnaire_questionnaire_id_foreign` FOREIGN KEY (`questionnaire_id`) REFERENCES `questionnaires` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question_questionnaire`
--

LOCK TABLES `question_questionnaire` WRITE;
/*!40000 ALTER TABLE `question_questionnaire` DISABLE KEYS */;
INSERT INTO `question_questionnaire` VALUES (1,90),(2,90),(3,90),(4,90),(5,90),(10010,91),(6,92);
/*!40000 ALTER TABLE `question_questionnaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaireresponses`
--

DROP TABLE IF EXISTS `questionnaireresponses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaireresponses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `researcher_id` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `response` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=533 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaireresponses`
--

LOCK TABLES `questionnaireresponses` WRITE;
/*!40000 ALTER TABLE `questionnaireresponses` DISABLE KEYS */;
INSERT INTO `questionnaireresponses` VALUES (6,1,3,'Multi-choice'),(7,1,4,'Open-ended'),(8,1,10001,'1'),(9,1,10016,'5'),(10,1,10025,'Likely'),(11,1,7,'Because they are very clear in what they are asking you. This means that the questions aren’t confusing so you won’t have any problems answering them.'),(12,1,1,'Likert scale'),(13,1,3,'Multi-choice'),(14,1,4,'Open-ended'),(15,1,10003,'3'),(16,1,10016,'5'),(17,1,10026,'Very likely'),(18,1,7,'These questions allow quantitative data to be collected, which is essential to display the results in relevant graphs.'),(19,1,3,'Multi-choice'),(20,1,4,'Open-ended'),(21,1,10001,'1'),(22,1,10021,'10'),(23,1,10024,'Undecided'),(24,1,7,'I\'m not sure what sort of questionnaire I am making and so that would determine what type of questions I would use.'),(25,1,3,'Multi-choice'),(26,1,4,'Open-ended'),(27,1,10001,'1'),(28,1,10016,'5'),(29,1,10023,'Unlikely'),(30,1,7,'The questions may not be relevant for inclusion in my questionnaire.'),(31,1,1,'Likert scale'),(32,1,3,'Multi-choice'),(33,1,4,'Open-ended'),(34,1,10002,'2'),(35,1,10018,'7'),(36,1,10024,'Undecided'),(37,1,7,'The questions I would use may depend upon the type of questionnaire and subject.'),(277,1,10033,'Testing 123'),(278,1,10033,'Testing 123'),(532,1,10033,'Testing 123.');
/*!40000 ALTER TABLE `questionnaireresponses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `researcher_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `ethical_considerations` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (90,1,'Questionnaire design and its use in research','Whenever you create questionnaires, whether they be on paper form or on websites such as SurveyMonkey, you will have noticed that some questions require an answer from a selection of answers, while other questions require an answer from your own choice of words. This is known as questionnaire design. Questionnaire design also takes into account if people prefer to answer closed-ended questions or open-ended questions as well as the length of an average question, closed-ended answers and open-ended responses, and whether people prefer to select one or more answers for a closed-ended question. This questionnaire will provide the opportunity for people who partake in questionnaires to be able to answer the questions and express some of their answers on what they think about questionnaires today.','All data in this questionnaire is anonymous and will only be shared between the questionnaire owner (David Coope), and the Edge Hill University computing department staff. The responses from this questionnaire will be compared with the secondary research to identify whether the Questionnaire 4 Us questionnaire system is suitable for people to partake in questionnaires or not. If you have any concerns regarding the questionnaire or your use of data within the questionnaire, please contact the questionnaire owner (David Coope) at david.coope@go.edgehill.ac.uk.','2016-05-07 02:48:00','2016-06-09 22:59:00'),(91,1,'Test','Testing 123','Test 12345','2016-04-27 08:00:00','2016-05-29 08:00:00'),(92,1,'Favourite Food?','A questionnaire on your favourite food.','I consider the ethics','2016-04-28 10:46:00','2016-04-28 11:00:00');
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `position_number` int(10) NOT NULL,
  `question` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_type_id_foreign_idx` (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10011 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (1,3,1,'Are you aware of the following question types? Please select all that apply.'),(2,2,2,'On a scale of 0-10 (0 = 0 words, 10 = 100 words), how many words do you expect to see in a question?'),(3,2,3,'On a scale of 0-10 (0 = 0 words, 10 = 10 words), how many words do you expect to see in an answer?'),(4,1,4,'Would you now consider any of the above questions if you created a questionnaire?'),(5,4,5,'What is your reasoning for choosing the selected answer from question 4?'),(6,3,1,'Favourite food?'),(10010,4,1,'Another Test');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questiontypes`
--

DROP TABLE IF EXISTS `questiontypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questiontypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questiontypes`
--

LOCK TABLES `questiontypes` WRITE;
/*!40000 ALTER TABLE `questiontypes` DISABLE KEYS */;
INSERT INTO `questiontypes` VALUES (1,'Closed-ended likert scale'),(2,'Closed-ended net promoter score'),(3,'Closed-ended multi-select'),(4,'Open-ended');
/*!40000 ALTER TABLE `questiontypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1),(2,1),(2,2);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'administrator'),(2,'researcher');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@example.com','$2y$10$HpTmyHvlmtRaS6zCCJjBPuTMnMf6i/tuold8I2GgnfkXwHmEJhdcS','a8119qjdwn4111P45tW0LMjtHrTgCebwTR2mqspopSpHOfXI5IXMAv3YTp1a','2016-03-31 00:28:37','2016-05-09 23:51:13'),(2,'researcher@example.com','$2y$10$l9qXkIF86hIqe3g0Nbl7FOKymyLU4FmwshGqmr8Qa9/6K4oDF9SsW','n6yHT95C5aIhzcgKYqgncJG1P7F3lxhdLTiRGUeMbWKI7qaAFztZLNL2E77X','2016-03-31 00:33:15','2016-05-09 18:17:06');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-10  1:54:52
